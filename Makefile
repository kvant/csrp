all:
	make -C src

help:
	make -C src help

clean:
	make -C src clean
install:
	make -C src install
