/* Copyright (C) 
 * 2013 - KVANT-Radiolocation
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */



#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "csrp.h"

/* Features */
#define FIFO_SW_SIMULATION	1
#define CHR_DEV_NAME		CSRP_DEV_NAME
#define PROC_ENTRY_NAME		CHR_DEV_NAME
#define PROC_ENTRY_ZS_NAME	"zone_schedulers"
#define PROC_ENTRY_ZONES_NAME	"zones"
#define PROC_ENTRY_GS_NAME	"global_schedulers"

static int major;
static char msg[200];
static char buf[200];

static struct proc_dir_entry *proc_csrp_dir;
static struct proc_dir_entry *proc_zs_dir;
static struct proc_dir_entry *proc_zones_dir;
static struct proc_dir_entry *proc_gs_dir;


static struct csrp_device
{
	int major;
} csrp_device;

struct zs_algo_rec {
	zone_schedulder_algo_t *algo;
	struct proc_dir_entry *proc;
	struct list_head link;
};

struct zs_zone_rec {
	uint32_t idx;
	char lid[64];
	struct zone_scheduler *zs;
	struct list_head link;
};

struct global_scheduler {
	struct global_schedulder_algo *algo;
	struct proc_dir_entry *proc;
};

static struct list_head zs_algo_list;
static struct list_head iz_algo_list;
static struct list_head zs_zones_list;
static struct global_scheduler global_sched;


#if FIFO_SW_SIMULATION

#endif

static inline struct zs_zone_rec *find_zone_by_name(const char *name)
{

	struct zs_zone_rec *zone = NULL;

	list_for_each_entry(zone, &zs_zones_list, link) {
		if (!strcmp(zone->lid, name))
			return zone;
	}

	return NULL;
}

static inline struct zs_zone_rec *find_zone_by_idx(uint32_t idx)
{

	struct zs_zone_rec *zone = NULL;

	list_for_each_entry(zone, &zs_zones_list, link) {
		if (zone->idx == idx)
			return zone;
	}

	return NULL;
}


static inline struct zs_algo_rec *find_zs_by_name(const char *name)
{

	struct zs_algo_rec *algo = NULL;

	list_for_each_entry(algo, &zs_algo_list, link) {
		if (!strcmp(algo->algo->name, name))
			return algo;
	}

	return NULL;
}

/**
 * @brief 
 *
 * @param new_algo
 * @param proc_dir_parent
 *
 * @return 
 */
int gs_algo_register(global_schedulder_algo_t *new_algo,
				struct proc_dir_entry **proc_dir_parent)
{
	int res = 0;

	struct zs_zone_rec *zone = NULL;

	global_sched.algo = new_algo;
	global_sched.proc = proc_mkdir(new_algo->name, proc_gs_dir);

	if(global_sched.proc == NULL) {
		res = -ENOMEM;
		pr_err("Out of memory here: %s:%u\n", __func__, __LINE__);
		return res;
	}

	if (proc_dir_parent)
		*proc_dir_parent = global_sched.proc;

	/*
	 * Init call
	 */

	if (global_sched.algo->init)
		if ((res = global_sched.algo->init(global_sched.algo)))
			return res;

	list_for_each_entry(zone, &zs_zones_list, link) {
		if (global_sched.algo->zone_add(global_sched.algo
					, zone->zs
					, zone->idx)) {
			pr_err("Error adding zone to global scheduler\n");
		}
	}


	return res;
}

/**
 * @brief 
 *
 * @param name
 *
 * @return 
 */
int gs_algo_unregister(const char *name)
{
	global_sched.algo = NULL;
	remove_proc_entry(name, proc_gs_dir);
	return 0;
}

int zs_algo_register(zone_schedulder_algo_t *new_algo,
					struct proc_dir_entry **proc_parent)
{
	int ret = 0;
	struct zs_algo_rec* algo;

	/* Check if algorithm already in the system */

	list_for_each_entry(algo, &zs_algo_list, link) {
		if (!strcmp(algo->algo->name, new_algo->name)) {
			pr_err("Zone scheduling algo with name %s already"
						" present\n", new_algo->name);
			ret = -EINVAL;
			goto out;
		}
	}

	/* Attach the algorithm */

	if (!(algo = kmalloc(sizeof(*algo), GFP_KERNEL))) {
		ret = -ENOMEM;
		pr_err("Out of memory here: %s:%u\n", __func__, __LINE__);
		goto out;
	}

	algo->algo = new_algo;
	algo->proc = proc_mkdir(new_algo->name, proc_zs_dir);
	if(algo->proc == NULL) {
		ret = -ENOMEM;
		kfree(algo);
		pr_err("Out of memory here: %s:%u\n", __func__, __LINE__);
		goto out;
	}

	if (proc_parent)
		*proc_parent = algo->proc;

	list_add(&algo->link, &zs_algo_list);

out:
	return ret;
}
int zs_algo_unregister(const char *name)
{
	int ret = 0;
	struct zs_algo_rec  *algo, *res = NULL;
	/* Check if algorithm already in the system */

	list_for_each_entry(algo, &zs_algo_list, link) {
		if (!strcmp(algo->algo->name, name)) {
			res = algo;
		}
	}

	return ret;
}

EXPORT_SYMBOL(gs_algo_register);
EXPORT_SYMBOL(gs_algo_unregister);
EXPORT_SYMBOL(zs_algo_register);
EXPORT_SYMBOL(zs_algo_unregister);

static ssize_t csrp_read(struct file *filp, char __user *buffer, size_t length,
								loff_t *offset)
{
  	return simple_read_from_buffer(buffer, length, offset, msg, 200);
}

static ssize_t csrp_write(struct file *filp, const char __user *buff,
							size_t len, loff_t *off)
{
	return len;
}

static int csrp_rfadd_ioctl_cb(input_data_t *idata) {

	int res = 0;
	struct zs_zone_rec *new_zs;
	struct zs_algo_rec *algo;

	if (find_zone_by_idx(idata->idx)) {
		pr_err("Zone with index %d already registered\n", idata->idx);
		return -EINVAL;
	}

	if (!(algo = find_zs_by_name(idata->zsched_name))) {
		pr_err("Zone algo %s not found\n", idata->zsched_name);
		res = -EINVAL;
		goto exit;
	}

	new_zs = kzalloc(sizeof *new_zs, GFP_KERNEL);

	if (!(new_zs->zs =
		algo->algo->zone_probe((rf_data_header_t *)idata->data
					, idata->idx))) {
		pr_err("Error zone probe \n");
		kfree(new_zs);
		res = -EINVAL;
	}

	strncpy(new_zs->lid, idata->zone_name, sizeof(new_zs->lid));
	new_zs->idx = idata->idx;

	list_add(&new_zs->link, &zs_zones_list);

	if (global_sched.algo) {
			res = global_sched.algo->zone_add(global_sched.algo
							, new_zs->zs
							, new_zs->idx);
			if (res) {
				pr_err("Error adding zone to global scheduler\n");
			}
	}

exit:
	return res;
}

/**
 * @brief 
 *
 * @param arg
 *
 * @return 
 */
static int csrp_zsget_ioctl_cb(struct sched_cmd_array *arg)
{
	struct zs_zone_rec *zrec;
	if(!(zrec = find_zone_by_idx(arg->idx))) {
		pr_err("Zone with index %d not in the system\n", arg->idx);
		return -EINVAL; }

//	zrec->zs->get_cmds(zrec->zs, &arg->carr, arg->carr.actual_size);

	return 0;
}

/**
 * @brief 
 *
 * @param f
 * @param cmd
 * @param arg
 *
 * @return 
 */
long csrp_ioctl(struct file *f, unsigned int cmd, unsigned long arg)
{
	int res = 0;
	switch(cmd) {

	case CSRP_IO_CRESET:
		res = copy_to_user((char *)arg, buf, 200);
		break;

	case CSRP_IOC_QINSERT:
		pr_err("Review request IOCTL\n");
		if (global_sched.algo)
			res = global_sched.algo->process_req(global_sched.algo
				   			, (struct tia_req *) arg);
		break;

	case CSRP_IOC_RFDADD:
		pr_err("RadioField add IOCTL\n");
		res = csrp_rfadd_ioctl_cb((input_data_t *) arg);
		break;

	case CSRP_IOC_ZSGET:
		pr_err("Zone Plan Get  IOCTL\n");
		res = csrp_zsget_ioctl_cb((struct sched_cmd_array *) arg);
		break;

	case CSRP_IOC_GSGET:
		pr_err("Global Plan Get  IOCTL\n");
		if (global_sched.algo)
			res = global_sched.algo->get_cmds(global_sched.algo
				   	, &((struct sched_cmd_array *) arg)->carr
					, ((struct sched_cmd_array *) arg)->idx);
		else
			pr_err("GS algo not present yet.\n");
		break;

	case CSRP_IOC_GSCALL:
		pr_err("GS call  IOCTL\n");
		if (global_sched.algo)
			res = global_sched.algo->ioctl(global_sched.algo
				   	, (struct gs_var *) arg);
		else
			pr_err("GS algo not present yet.\n");

		break;
	default:
		pr_err("Undefined command: %d \n", cmd);
		return -ENOTTY;
	}

	return res;
}

static struct file_operations csrp_fops = {
	.read = csrp_read,
	.write = csrp_write,
	.unlocked_ioctl = csrp_ioctl,
};

static int __init csrp_init(void)
{
	major = register_chrdev(0, CHR_DEV_NAME, &csrp_fops);
	if (major < 0) {
		printk ("Registering the CSRP failed with %d\n", major);
		return major;
	}
	csrp_device.major = major;

	/* Create procfs main directory */

	proc_csrp_dir = proc_mkdir(PROC_ENTRY_NAME, NULL);
	if(proc_csrp_dir == NULL) {
		unregister_chrdev(major, CHR_DEV_NAME);
		return -ENOMEM; 
	}

	proc_zs_dir = proc_mkdir(PROC_ENTRY_ZS_NAME, proc_csrp_dir);
	if(proc_zs_dir == NULL) {
		unregister_chrdev(major, CHR_DEV_NAME);
		return -ENOMEM; 
	}

	proc_zones_dir = proc_mkdir(PROC_ENTRY_ZONES_NAME, proc_csrp_dir);
	if(proc_zones_dir == NULL) {
		unregister_chrdev(major, CHR_DEV_NAME);
		return -ENOMEM; 
	}

	proc_gs_dir = proc_mkdir(PROC_ENTRY_GS_NAME, proc_csrp_dir);
	if(proc_gs_dir == NULL) {
		unregister_chrdev(major, CHR_DEV_NAME);
		return -ENOMEM; 
	}

	INIT_LIST_HEAD(&zs_algo_list);
	INIT_LIST_HEAD(&iz_algo_list);
	INIT_LIST_HEAD(&zs_zones_list);

	printk("CSRP: assigned major: %d\n", major);
	printk(" Please create node with mknod /dev/csrp c %d 0\n", major);

	return 0;
}

static void __exit csrp_exit(void)
{
	remove_proc_entry(PROC_ENTRY_GS_NAME, proc_csrp_dir);
	remove_proc_entry(PROC_ENTRY_ZS_NAME, proc_csrp_dir);
	remove_proc_entry(PROC_ENTRY_ZONES_NAME, proc_csrp_dir);
	remove_proc_entry(PROC_ENTRY_NAME, NULL);

	unregister_chrdev(major, CHR_DEV_NAME);
}

module_init(csrp_init);
module_exit(csrp_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Riazantsev Vladimir <riazantsev.v@gmail.com>");
MODULE_DESCRIPTION("Command Scheduled and Review Planner");

