/* Copyright (C)
 * 2013 - KVANT-Radiolocation
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */



#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/hrtimer.h>

#include "csrp.h"

struct hrtimer timer1;

static enum hrtimer_restart timer_tick_func(struct hrtimer *timer)
{
        ktime_t tim = ktime_set(1, 0);
        ktime_t now = hrtimer_cb_get_time(&timer1);

	pr_err(">>> timer: %lu\n", now.tv64);

	hrtimer_start(&timer1, tim, HRTIMER_MODE_REL);
	return HRTIMER_NORESTART;
}

struct hrtimer timer1 = {
	.function = timer_tick_func,
};

static int __init fe_init(void)
{
        ktime_t tim = ktime_set(1, 0);

	hrtimer_init(&timer1, CLOCK_MONOTONIC, HRTIMER_MODE_REL);

	timer1.function = timer_tick_func;

	hrtimer_start(&timer1, tim, HRTIMER_MODE_REL);

	return 0;
}

static void __exit fe_exit(void)
{

}

module_init(fe_init);
module_exit(fe_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Riazantsev Vladimir <riazantsev.v@gmail.com>");
MODULE_DESCRIPTION("Command Scheduled and Review Planner");

