#ifndef __IMPORT_UTILS_H__
#define __IMPORT_UTILS_H__

#include <linux/time.h>
#include "inex_types.h"



/*
 * Some math definitions
 */

# define M_E		2.7182818284590452354	/* e */
# define M_LOG2E	1.4426950408889634074	/* log_2 e */
# define M_LOG10E	0.43429448190325182765	/* log_10 e */
# define M_LN2		0.69314718055994530942	/* log_e 2 */
# define M_LN10		2.30258509299404568402	/* log_e 10 */
# define M_PI		3.14159265358979323846	/* pi */
# define M_PI_2		1.57079632679489661923	/* pi/2 */
# define M_PI_4		0.78539816339744830962	/* pi/4 */
# define M_1_PI		0.31830988618379067154	/* 1/pi */
# define M_2_PI		0.63661977236758134308	/* 2/pi */
# define M_2_SQRTPI	1.12837916709551257390	/* 2/sqrt(pi) */
# define M_SQRT2	1.41421356237309504880	/* sqrt(2) */
# define M_SQRT1_2	0.70710678118654752440	/* 1/sqrt(2) */

# define ABS(X) ((X) < 0 ? (-1 * (X)) : (X))
# define MAX(X, Y) ((X) >= (Y) ? (X) : (Y))
# define MIN(X, Y) ((X) <= (Y) ? (X) : (Y))
# define INRANGE(X, Y, Z) \
	((((X) <= (Y)) && ((Y) <= (Z))) || \
	(((Z) <= (Y)) && ((Y) <= (X))) ? 1 : 0)


/**
 * @brief 
 *
 * @param data
 * @param field
 *
 * @return 
 */
int rf_data_parse_t1(rf_data_header_t *data, rf_field_t **field);

/**
 * @brief 
 *
 * @param field
 *
 * @return 
 */
int rf_data_free_t1(rf_field_t *field);

/**
 * @brief 
 *
 * @param field
 *
 * @return 
 */
int rf_data_dump_t1(rf_field_t *field);


/** Time utils **/

static inline struct timespec usec_to_ts(u64 usec)
{
	return ns_to_timespec(usec * 1000);
}

static inline void ts_add_usec(struct timespec *ts, u64 usec)
{
	timespec_add_ns(ts, usec * 1000);
}

static inline void ts_sub_usec(struct timespec *ts, u64 usec)
{
	*ts = timespec_sub(*ts, ns_to_timespec(usec * 1000));
}


static inline u64 ts_to_usec(struct timespec *ts)
{
	s64 nsec = timespec_to_ns(ts);
	nsec /= 1000;
	return nsec;
}


#endif
