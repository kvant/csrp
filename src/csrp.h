#ifndef __CSRP_H__
#define __CSRP_H__

#include <linux/list.h>
#include <linux/proc_fs.h>

#include "import_utils.h"

#define MAX_ZONE_NAME	64
#define MAX_SCHED_NAME	64

#define CPSR_ERR(x) pr_err x


typedef struct zone_stats {
	char z_name[MAX_ZONE_NAME];
	char s_name[MAX_ZONE_NAME];
	uint32_t duration;
	uint32_t cmds_num;
	rangei_t d_range;
	rangef_t a_range;
	rangef_t e_range;
} zone_stats_t;

/**
 * @brief Zone scheduler
 */
typedef struct zone_scheduler {
	int (*get_stats)(struct zone_scheduler *zs
						, struct zone_stats *stats);

	int (*get_cmds)(struct zone_scheduler *zs
						, struct paa_cmd_array *cmds
						, unsigned max_count
						, unsigned  *max_duration);

	int (*layer_to_cmd)(struct zone_scheduler *zs
						, unsigned layer_id[2]
						, unsigned *cmd_b
						, unsigned *cmd_e
						, unsigned *duration);

	int (*seek)(struct zone_scheduler *zs
						, unsigned cmd_id);

	int (*get_cmd_by_coords)(struct zone_scheduler *zs
						, struct paa_cmd *cmd
						, double a
						, double e);

	int (*reset)(void);

	uint32_t (*time_to_end)(struct zone_scheduler *zs);

	void *priv;
} zone_scheduler_t;

/**
 * @brief Zone scheduler algorithm
 */
typedef struct zone_schedulder_algo {
	              u8   name[MAX_SCHED_NAME];
	zone_scheduler_t  *(*zone_probe)(rf_data_header_t *rf_data
						, uint32_t idx);

		void   (*zone_destroy)(zone_scheduler_t * zs);
} zone_schedulder_algo_t;

/**
 * @brief Global scheduler algorithm
 */
typedef struct global_schedulder_algo {

	char name[MAX_SCHED_NAME];
	int (*init)(struct global_schedulder_algo *algo);

	int (*ioctl)(struct global_schedulder_algo *algo, struct gs_var *var);

	int (*zone_add)(struct global_schedulder_algo *algo
						, struct zone_scheduler *zs
						, uint32_t id);

	int (*get_cmds)(struct global_schedulder_algo *algo
						, struct paa_cmd_array *cmds
						, uint32_t count);

	int (*get_cmds_timed)(struct global_schedulder_algo *algo
						, struct paa_cmd_array *cmds
						, unsigned max_cnt
						, unsigned *duration_us
						, unsigned *next_duration);

	void (*zone_remove)(struct global_schedulder_algo *algo
						, uint32_t id);

	void (*zone_enable)(struct global_schedulder_algo *algo
						, uint32_t id
						, unsigned en);

	int (*process_req)(struct global_schedulder_algo *algo
						, struct tia_req *req);

	uint32_t (*time_to_end)(struct global_schedulder_algo *algo);

} global_schedulder_algo_t;

/**
 * @brief Inter-Zone scheduler
 */
typedef struct paa_scheduler {


} paa_scheduler_t;

/**
 * @brief Phased array antenna zone
 */
typedef struct paa_zone {
	uint32_t id;			/**< Zone unique id */
	paa_zone_flags_t flags;		/**< Flags @see zone_flags_t */
	char name[MAX_ZONE_NAME];	/**< Zone ASCII name */
	rangef_t azimuth;			/**< Azimuth range */
	rangef_t elevation;		/**< Elevation range */
	rangef_t distance;		/**< Distance range */
	char  scheduler;	/**< Scheduler */
} paa_zone_t;

/**
 * @brief Phased array antenna plane
 */
struct paa_plane {
	uint32_t id;
	struct list_head zones;
	paa_scheduler_t scheduler;
};


extern int zs_algo_register(zone_schedulder_algo_t *new_algo,
				struct proc_dir_entry **proc_dir_parent);
extern int zs_algo_unregister(const char *name);

extern int gs_algo_register(global_schedulder_algo_t *new_algo,
				struct proc_dir_entry **proc_dir_parent);
extern int gs_algo_unregister(const char *name);


/* Features */
#define FIFO_SW_SIMULATION	1


#endif
