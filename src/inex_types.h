#ifndef __INEX_TYPES_H__
#define __INEX_TYPES_H__

#define CSRP_DEV_NAME	"csrp"

/* Use 'k' as magic */
#define CSRP_IOC_MAGIC 'k'

/* Reset */
#define CSRP_IO_CRESET		_IO(CSRP_IOC_MAGIC,	0)
#define CSRP_IOC_QINSERT	_IOW(CSRP_IOC_MAGIC,	1, int)
#define CSRP_IOC_RFDADD		_IOW(CSRP_IOC_MAGIC,	2, void *)
#define CSRP_IOC_ZSGET		_IOWR(CSRP_IOC_MAGIC,	3, void *)
#define CSRP_IOC_GSCALL		_IOWR(CSRP_IOC_MAGIC,	4, void *)
#define CSRP_IOC_GSGET		_IOWR(CSRP_IOC_MAGIC,	5, void *)

#define usec_to_nsec(x) ((x) * 1000)
#define nsec_to_usec(x) ((x) / 1000)

/*
 * S means "Set" via ptr,
 * T means "Tell" via arg
 * G means "Get"
 * Q means "Query"
 * X means "eXchange"
 * H means "sHift"
 * U meanth "Update"
 */

#define CSRP_IOC_MAXNR 2

/**
 * @brief 
 */
typedef struct rf_alayer {
	uint32_t	idx;
	uint32_t	rp_steps;		/**< Radiation pattern */
	double		*values;
} __attribute__ ((packed)) rf_alayer_t;

/**
 * @brief 
 */
typedef struct rf_elayer {
	uint32_t	idx;
	double		e_val;
	uint32_t	rp_steps;
	uint8_t		*values;
} __attribute__ ((packed)) rf_elayer_t;

/**
 * @brief 
 */
typedef struct rf_egroup {
	uint32_t	idx;
	uint32_t	elayers_cnt;
	rf_elayer_t	*elayers;
} __attribute__ ((packed)) rf_group_t;

/**
 * @brief 
 */
typedef struct rf_field {
	uint8_t		shrt_name[16];
	uint8_t		name[64];
	uint32_t	range_scale;
	double		probability;
	uint32_t	pulse_duration;
	uint32_t	pulse_period;
	uint32_t	elayer_per_gr;
	uint32_t	pulse_cnt;
	uint32_t	alayer_cnt;
	uint32_t	egroup_cnt;
	rf_alayer_t	*alayers;
	rf_group_t	*egroups;
} __attribute__ ((packed)) rf_field_t;

/**
 * @brief 
 */
enum rf_data_type_t {
	RFDT_FIXED_TABLE = 0x64,
};

/**
 * @brief 
 */
typedef struct rf_data_header {
	uint32_t type;
	uint32_t version;
	uint32_t size;
	uint8_t data[];
} rf_data_header_t;

/**
 * @brief 
 */
typedef struct input_data {
	uint32_t type;
	uint32_t version;
	uint32_t idx;
	char zsched_name[64];
	char zone_name[64];
	size_t len;
	uint8_t data[];
} __attribute__ ((packed)) input_data_t;

/**
 * @brief Zone flags
 */
typedef enum zone_flags {
	ZF_ACTIVE,	/**< Zone is in active state */
	ZF_BLANK,	/**< Zone is blanked */
} paa_zone_flags_t;

/**
 * @brief Range index
 */
enum range_index {
	RANGE_MIN,	/**< Index for min item */
	RANGE_MAX	/**< Index for max item */
};

/**
 * @brief Type for define range of double values
 */
typedef double rangef_t[2];
typedef uint32_t rangei_t[2];

/**
 * @brief Radio freq data
 */
typedef struct rf_data {
	uint32_t	imp_count;	/**< Count of impulses */
	uint32_t	imp_period;	/**< Impulses period */
	uint32_t	imp_duration;	/**< Impulses duration */
	uint32_t	imp_power;	/**< Power in impulse */
	uint32_t	rf_mode;	/**< Mode */
} __attribute__ ((packed)) rf_data_t;

/**
 * @brief Phased array antenna command
 */

#define CMD_ZONE_SHIFT		24
#define CMD_ZONE_MASK		0xFF
#define CMD_ID_MASK		(BIT(CMD_ZONE_SHIFT) - 1)
#define CMD_ZONE_VAL(zone)	(((zone) & CMD_ZONE_MASK) << CMD_ZONE_SHIFT)

#define CMD_SET_ID(_cmd, _id) \
	do { \
		_cmd->id &= ~(CMD_ID_MASK); \
		_cmd->id |= (_id & CMD_ID_MASK); \
	} while(0)

#define CMD_SET_ZONE(cmd, zone) \
	do { \
		cmd->id &= ~(CMD_ZONE_MASK << CMD_ZONE_SHIFT); \
		cmd->id |= (zone & CMD_ZONE_MASK) << CMD_ZONE_SHIFT; \
	} while(0)

#define CMD_GET_ZONE(cmd) ((cmd->id >> CMD_ZONE_SHIFT) & CMD_ZONE_MASK)

struct paa_cmd {
	uint32_t id;			/**< Unique command id */
	double elevation;		/**< Elevation value */
	double azimuth;			/**< Azimuth value */
	rf_data_t rf_data;		/**< Data */
#define cmd_get_duration(cmd) ((cmd)->rf_data.imp_count * (cmd)->rf_data.imp_period)
} __attribute__ ((packed));


/**
 * @brief 
 */
struct paa_cmd_array {
	uint32_t efective_size;
	uint32_t actual_size;
	uint32_t duration_us;
#define CAF_APPEND	BIT(0)
#define CAF_NO_LOOP	BIT(1)
	uint32_t flags;
	struct paa_cmd cmd[];
};

/**
 * @brief 
 */
struct sched_cmd_array {
	uint32_t idx;
	struct paa_cmd_array carr;
};

/**
 * @brief 
 */
struct gss_review_item {
	uint32_t zone_id;
	uint32_t item_type;
	union {
		uint32_t cmds[2]; /**< off, cnt */
	} data;
};

/**
 * @brief 
 */
struct gss_review_plan {
	uint32_t items_cnt;
	struct gss_review_item items[];
};

/* TODO: Temporary for test */

struct gss_mixer_test {
	struct timespec deadline;
	struct paa_cmd_array *cmd_arr;
	unsigned ncmds;
};

enum gs_var_type {
	 GSSETPLAN,
/* TODO: Temporary for test */
	 GSTESTMIXER,
};

/**
 * @brief 
 */
struct gs_var {
	uint32_t type;
	uint8_t data[];
};

/**
 * @brief
 */
enum req_type {
	REQ_INSERT,	/**< */
	REQ_DELETE,	/**< */
	REQ_UPDATE,	/**< */
};

/**
 * @brief 
 */
struct tia_req {
	uint32_t id;			/**< ID of request */
	uint32_t ilevel;		/**< Priority level */
	uint32_t flags;			/**< Processing flags */
	uint32_t period;		/**< Request period, usec */
	uint32_t repeats;		/**< Number of repeats (not used) */
	uint32_t max_deviation;		/**< Maximal deviation, usec */
	struct timespec next_time;	/**< Start time  */
	double x, y, z;			/**< Coordinates. Decart. */
	double a, e, d;			/**< Coordinates. Spherical. */
	double vx, vy, vz;		/**< Velocities */
};

#endif
