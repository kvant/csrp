/* Copyright (C)
 * 2013 - KVANT-Radiolocation
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */


#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/seq_file.h>

#include "csrp.h"

#define VERSION_MAJOR	0
#define VERSION_MINOR	1
#define ZS_SIMPLE_NAME	"simple"

enum {
	PROC_VERSION,
	PROC_LAST,
};

static struct proc_dir_entry *proc_parent, *proc_version;

struct a_layer {
	uint32_t idx;
	uint32_t steps;
	rangef_t a_range;
	uint32_t duration;
	uint32_t half:1;
	double e_val;
	struct paa_cmd *cmds;
};

struct e_layer {
	uint32_t idx;
	uint32_t alayer_cnt;
	time_t duration;
	rangef_t e_range;
	rangef_t a_range;
	struct a_layer *alayers;
};

struct	cmd_ptr {
#define CPF_INVERT_A	BIT(0)
#define CPF_INVERT_D	BIT(1)
#define CPF_SKIP	BIT(2)
#define CPF_FINISH	BIT(3)
	uint32_t mflags;
	uint32_t tte;
	uint32_t duration;
	union {
		uint32_t cmd_idx;
		struct paa_cmd *pcmd;
	} cmd;
};

struct scan_zone {
	uint32_t gidx;
	char shrt_name[16];
	char name[64];
	rangei_t d_range;
	rangef_t e_range;
	rangef_t a_range;
	double probability;
	uint32_t p_duration;
	uint32_t p_period;
	uint32_t cmd_count;
	uint32_t sched_size;
	uint32_t layers_cnt;
	uint32_t elayers_cnt;
	struct e_layer *elayers;
	struct paa_cmd *cmds;
	struct cmd_ptr *cptrs;

	unsigned curr_cmd;
	uint32_t z_duration;
	uint32_t tte; /* Time Till End */
};

struct zs_simple_object {
	struct scan_zone zone;
	struct zone_scheduler zs;
	struct list_head node;
};

static struct list_head zs_objects;

struct proc_ops_specific {
	__u32 id;
	int (*show)(struct seq_file *s, void *unused);
};

static int proc_show_version(struct seq_file *s, void *unused)
{
	seq_printf(s, "Version: %d.%d\n", VERSION_MAJOR, VERSION_MINOR);
	return 0;
}

static int proc_file_open(struct inode *inode, struct file *file)
{
	struct proc_dir_entry *pde = PDE(inode);
	struct proc_ops_specific *data = pde->data;

	single_open(file, data->show, data);
	return 0;
}

static struct file_operations fops = {
	.open = proc_file_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

static struct proc_ops_specific proc_file_ops[] = {
	[PROC_VERSION] = {
		.id = PROC_VERSION,
		.show = proc_show_version,
	},
};

/**
 * @brief 
 *
 * @param zs
 * @param cmd_id
 *
 * @return 
 */
static int zss_seek_by_id(struct zone_scheduler *zs, unsigned cmd_id)
{
	struct zs_simple_object *zss = container_of(zs, struct zs_simple_object, zs);

	if (cmd_id > zss->zone.sched_size)
		return -EINVAL;

	zss->zone.curr_cmd = cmd_id;

	return 0;
}

/**
 * @brief 
 *
 * @param zs
 * @param cmds
 * @param count
 *
 * @return 
 */
static int zss_get_cmds(struct zone_scheduler *zs
					, struct paa_cmd_array *cmds
					, unsigned count
					, unsigned *duration)
{
	int ret;
	unsigned i = 0;
	unsigned len;
	struct paa_cmd *out = cmds->cmd;
	struct zs_simple_object *zss = container_of(zs, struct zs_simple_object, zs);

	if (!count)
		return count;

	out = cmds->cmd + cmds->actual_size;

	len = zss->zone.sched_size - zss->zone.curr_cmd + 1;

	if ((count > len) && (cmds->flags & CAF_NO_LOOP))
		count = len;

	ret = count;
	i = zss->zone.curr_cmd;

	if (duration) {
		len = *duration;

		while (len >= zss->zone.cptrs[i].duration && count) {
			*out = *(zss->zone.cptrs[i].cmd.pcmd);

			if (zss->zone.cptrs[i].mflags & CPF_INVERT_A)
				out->azimuth = -out->azimuth;

			len -= zss->zone.cptrs[i].duration;

			++out;
			--count;
			++i;
			i %= zss->zone.sched_size;
		}

		cmds->duration_us += (*duration - len);
		ret -= count;
		*duration = len;
	} else {
		do {
			*out = *(zss->zone.cptrs[i].cmd.pcmd);

			if (zss->zone.cptrs[i].mflags & CPF_INVERT_A)
				out->azimuth = -out->azimuth;

			cmds->duration_us += zss->zone.cptrs[i].duration;

			++out;
			++i;
			i %= zss->zone.sched_size;

		} while (--count);
	}

	zss->zone.curr_cmd = i;

	cmds->actual_size += ret;

	return ret;
}

static uint32_t zss_time_to_end(struct zone_scheduler *zs)
{

	return 0;
}

static int zss_reset(void)
{

	return 0;
}

static int zss_get_stats(struct zone_scheduler *zs, struct zone_stats *stats)
{
	struct zs_simple_object *zss = container_of(zs, struct zs_simple_object, zs);

	memset(stats, 0x00, sizeof *stats);

	memcpy(stats->z_name, zss->zone.name, sizeof(zss->zone.name));
	memcpy(stats->s_name, zss->zone.shrt_name, sizeof(zss->zone.shrt_name));

	stats->duration = zss->zone.p_duration;
	stats->cmds_num = zss->zone.cmd_count;

	stats->d_range[0] = zss->zone.d_range[0];
	stats->d_range[1] = zss->zone.d_range[1];

	stats->a_range[0] = zss->zone.a_range[0];
	stats->a_range[1] = zss->zone.a_range[1];

	stats->e_range[0] = zss->zone.e_range[0];
	stats->e_range[1] = zss->zone.e_range[1];

	pr_err("Stats: N:%s  SN:%s D:%u CN:%u D:[%u - %u] A:[%d - %d] E:[%d - %d] \n"
			, stats->z_name
			, stats->s_name
			, stats->duration
			, stats->cmds_num
			, stats->d_range[0]
			, stats->d_range[1]
			, (int)(stats->a_range[0] * 1000)
			, (int)(stats->a_range[1] * 1000)
			, (int)(stats->e_range[0] * 1000)
			, (int)(stats->e_range[1] * 1000)
			);

	return 0;
}
/*
static inline uint32_t cmd_get_duration(struct paa_cmd *cmd)
{
	return cmd->rf_data.imp_count
		* cmd->rf_data.imp_period;
}

*/
/**
 * @brief 
 *
 * @param zs
 * @param layer_id
 * @param cmd_id
 * @param begin
 *
 * @return 
 */
static int zss_layer_to_cmd(struct zone_scheduler *zs
					, unsigned layer_id[2]
					, unsigned *cmd_b
					, unsigned *cmd_e
					, unsigned *duration)
{
	unsigned i = 0;
	unsigned layer = 0;
	unsigned dur = 0;
	double elev;
	struct zs_simple_object *zss = container_of(zs, struct zs_simple_object, zs);

	pr_err(">>> %s from:%d till:%d total:%d \n", __func__, layer_id[0], layer_id[1], zss->zone.sched_size);

	if (layer_id[0] > zss->zone.layers_cnt
			|| layer_id[1] > zss->zone.layers_cnt
			|| layer_id[1] < layer_id[0]
			|| !(cmd_b && cmd_e))
		return -EINVAL;

	elev = zss->zone.cptrs[0].cmd.pcmd->elevation;

	if (!layer_id[0]) {
		*cmd_b = 0;
		goto skip_0;
	}

	for(; i < zss->zone.sched_size; i++) {
		if (elev != zss->zone.cptrs[i].cmd.pcmd->elevation) {
			elev = zss->zone.cptrs[i].cmd.pcmd->elevation;
			++layer;
			if (layer == layer_id[0]) {
				*cmd_b = i;
				i++;
				break;
			}
		}
	}

	if (i == zss->zone.sched_size) {
		pr_err("Can't find start of layer\n");
		return -EINVAL;
	}
	pr_err("B:%d\n", *cmd_b);
skip_0:

	/* search for end */
	for(; i < zss->zone.sched_size; i++) {
		dur += cmd_get_duration(zss->zone.cptrs[i].cmd.pcmd);
		if (elev != zss->zone.cptrs[i].cmd.pcmd->elevation) {
			elev = zss->zone.cptrs[i].cmd.pcmd->elevation;
			if (layer == layer_id[1]) {
				*cmd_e = i - 1;
				break;
			}
			++layer;
		}
	}

	*duration = dur;

	/* finish */
	if (i == zss->zone.sched_size) {
		if ((zss->zone.layers_cnt - 1) == layer_id[1]) {
			*cmd_e = i - 1;
		} else {
			pr_err("Can't find requested layer end\n");
			return -EINVAL;
		}
	}

	pr_err("B:%d E:%d D:%d \n", *cmd_b, *cmd_e, *duration);

	return 0;
}

/**
 * @brief 
 *
 * @param zone
 * @param flags
 *
 * @return 
 */
static int zs_simple_zone_prefill(struct scan_zone *zone, uint32_t flags)
{
	unsigned i = 0, j, k, invert, ltte;
	struct cmd_ptr *pcmd;

	zone->sched_size = 0;
	zone->layers_cnt = 0;

	/* Counting total number of command in sched. plan and total duration */

	for(; i < zone->elayers_cnt; i++) {
		zone->elayers[i].duration = 0;
		for (j = 0; j < zone->elayers[i].alayer_cnt; j++) {
			zone->sched_size += zone->elayers[i].alayers[j].steps * 2;
			zone->elayers[i].duration += zone->elayers[i].alayers[j].duration * 2;
			if (0.0 == zone->elayers[i].alayers[j].cmds[0].azimuth) {
				zone->sched_size--;
				zone->elayers[i].duration -= cmd_get_duration(zone->elayers[i].alayers[j].cmds);
			}
			++zone->layers_cnt;
		}
	}

	pcmd = zone->cptrs = kzalloc(zone->sched_size * sizeof *zone->cptrs
								, GFP_KERNEL);
	if (!pcmd) {
		pr_err("Out of memory here: %s:%d\n", __func__, __LINE__);
		return -ENOMEM;
	}

	zone->curr_cmd = 0;

	invert = 0;
	for(i = 0; i < zone->elayers_cnt; i++ ) {
		ltte = zone->elayers[i].duration;
		for (j = 0; j < zone->elayers[i].alayer_cnt; j++) {

			/* Mirroring */
			for(k = zone->elayers[i].alayers[j].steps; k-- ; pcmd++) {
				pcmd->mflags = invert ? CPF_INVERT_A : 0;
				pcmd->cmd.pcmd = zone->elayers[i].alayers[j].cmds + k;
				pcmd->tte = ltte;
				ltte -= (pcmd->duration = cmd_get_duration(pcmd->cmd.pcmd));
			}

			k = zone->elayers[i].alayers[j].cmds[0].azimuth == .0 ? 1 : 0;
			invert ^= 1;

			/* Mirroring */
			for(; k < zone->elayers[i].alayers[j].steps; k++, pcmd++) {
				pcmd->mflags = invert ? CPF_INVERT_A : 0;
				pcmd->cmd.pcmd = zone->elayers[i].alayers[j].cmds + k;
				pcmd->tte = ltte;
				ltte -= (pcmd->duration = cmd_get_duration(pcmd->cmd.pcmd));
			}
		}
	}

	pr_err("Duration = %lu uS\n", zone->elayers[0].duration);
	pr_err("Total commands = %d\n", zone->sched_size);

	return 0;

}

#define bsearch_fuzzy(aa, bb, mm, p, mem, val)		\
	do {						\
		mm = aa + ((bb - aa) >> 1);		\
		if ((p)[mm].mem <= val) {		\
			if ((p)[mm + 1].mem > val) {	\
				break;			\
			}				\
			aa = mm + 1;			\
		} else {				\
			bb = mm;			\
		}					\
	} while (aa < bb);

static int zss_get_cmd_by_coords(struct zone_scheduler *zs, struct paa_cmd *cmd, double a, double e)
{
	struct zs_simple_object *zss = container_of(zs, struct zs_simple_object, zs);
	struct e_layer *layer =  &zss->zone.elayers[0];
	unsigned start = 0,
		 end = layer->alayer_cnt,
		 m,
		 l;

	struct {
		double val;
		struct paa_cmd *cmd;
	} cmds[4] = {{0, NULL}};



	if (a < 0)
		a = -a;

	/*
	 * Search for azimuth layer by elevation value
	 */
	bsearch_fuzzy(start, end, m, layer->alayers, e_val, e);

	start = 0;
	end = layer->alayers[m].steps;

	/*
	 * Search for azi
	 */
	bsearch_fuzzy(start, end, l, layer->alayers[m].cmds, azimuth, a);

	cmds[0].cmd = &layer->alayers[m].cmds[l];
	cmds[1].cmd = &layer->alayers[m].cmds[l + 1];

	++m;

	start = 0;
	end = layer->alayers[m].steps;

	bsearch_fuzzy(start, end, l, layer->alayers[m].cmds, azimuth, a);

	cmds[2].cmd = &layer->alayers[m].cmds[l];
	cmds[3].cmd = &layer->alayers[m].cmds[l + 1];

	l = 0;
	l = cmds[l].cmd->rf_data.imp_count > cmds[1].cmd->rf_data.imp_count ? l : 1;
	l = cmds[l].cmd->rf_data.imp_count > cmds[2].cmd->rf_data.imp_count ? l : 2;
	l = cmds[l].cmd->rf_data.imp_count > cmds[3].cmd->rf_data.imp_count ? l : 3;

	*cmd = *cmds[l].cmd;

#if 0
	pr_err(" >>> A=%u E=%u imp=%u \n", (unsigned)(cmds[0].cmd->azimuth * 1000)
						, (unsigned)(cmds[0].cmd->elevation * 1000)
						, cmds[0].cmd->rf_data.imp_count );

	pr_err(" >>> A=%u E=%u imp=%u \n", (unsigned)(cmds[1].cmd->azimuth * 1000)
						, (unsigned)(cmds[1].cmd->elevation * 1000)
						, cmds[1].cmd->rf_data.imp_count );

	pr_err(" >>> A=%u E=%u imp=%u \n", (unsigned)(cmds[2].cmd->azimuth * 1000)
						, (unsigned)(cmds[2].cmd->elevation * 1000)
						, cmds[2].cmd->rf_data.imp_count );

	pr_err(" >>> A=%u E=%u imp=%u \n", (unsigned)(cmds[3].cmd->azimuth * 1000)
						, (unsigned)(cmds[3].cmd->elevation * 1000)
						, cmds[3].cmd->rf_data.imp_count );

	pr_err(" >>> A=%u E=%u imp=%u \n", (unsigned)(cmd->azimuth * 1000)
						, (unsigned)(cmd->elevation * 1000)
						, cmd->rf_data.imp_count );
#endif

	return 0;
}

static zone_scheduler_t *zs_simple_zone_probe(rf_data_header_t *rf_data, uint32_t idx)
{
	unsigned i, j, k, layer, cmd = 0;
	rf_field_t *field;
	struct zs_simple_object *obj;
	struct scan_zone *new_zone;
	struct paa_cmd *pcmd;

	if(!rf_data_parse_t1(rf_data, &field)) {
		rf_data_dump_t1(field);
	}

	obj = kzalloc(sizeof *obj, GFP_KERNEL);
	INIT_LIST_HEAD(&obj->node);
	obj->zs.get_stats = zss_get_stats;
	obj->zs.reset = zss_reset;
	obj->zs.get_cmds = zss_get_cmds;
	obj->zs.time_to_end = zss_time_to_end;
	obj->zs.layer_to_cmd = zss_layer_to_cmd;
	obj->zs.seek = zss_seek_by_id;
	obj->zs.get_cmd_by_coords = zss_get_cmd_by_coords;
	obj->zs.priv = obj;

	new_zone = &obj->zone;

	if (!new_zone) {
		/*TODO: Error handling*/
		return NULL;
	}

	new_zone->gidx = idx; /*TODO:*/
	strncpy(new_zone->shrt_name, field->shrt_name,
					sizeof(new_zone->shrt_name));
	strncpy(new_zone->name, field->name,
					sizeof(new_zone->name));
	new_zone->d_range[0] = 0;
	new_zone->d_range[1] = field->range_scale;

	new_zone->probability = field->probability;
	new_zone->p_duration = field->pulse_duration;
	new_zone->p_period = field->pulse_period;

	/* Assume that we have just one elevation layer */
	new_zone->elayers_cnt = 1;
	new_zone->elayers = kzalloc(sizeof(new_zone->elayers[0]), GFP_KERNEL);

	/* Count commands number */

	if (field->egroup_cnt > field->alayer_cnt) {
		pr_err("Field can't be proceed due incorrect configuration\n");
		goto err;
	}

	for (i = 0; i < field->egroup_cnt; i++) {
		for(j = i & 1; j < field->egroups[i].elayers_cnt; j += 2) {
			/* ALayers count */
			new_zone->elayers[0].alayer_cnt++;
			/* Total command count */
			new_zone->cmd_count += field->egroups[i].elayers[j].rp_steps;
		}
	}

	pcmd = new_zone->cmds = kzalloc(new_zone->cmd_count
				* sizeof(new_zone->cmds[0])
				, GFP_KERNEL);

	pr_err("Overall commands count is: %d size: %lu\n"
				, new_zone->cmd_count
				, new_zone->cmd_count * sizeof(new_zone->cmds));

	/* Commands parsing */

	new_zone->elayers[0].alayers = kzalloc(new_zone->elayers[0].alayer_cnt
				* sizeof(new_zone->elayers[0].alayers[0])
				, GFP_KERNEL);

	layer = cmd = 0;

	for (i = 0; i < field->egroup_cnt; i++) {
		layer = j = i & 1; /* odd or even */

		for(; j < field->egroups[i].elayers_cnt; j += 2, layer += 2) {

			new_zone->elayers[0].alayers[layer].cmds = pcmd;

			new_zone->elayers[0].alayers[layer].steps =
					field->egroups[i].elayers[j].rp_steps;

			new_zone->elayers[0].alayers[layer].idx = layer;
			new_zone->elayers[0].alayers[layer].e_val =
					field->egroups[i].elayers[j].e_val;

			pr_err("El:%u Layer:%u\n", (uint32_t) (field->egroups[i].elayers[j].e_val * 1000), layer);

			new_zone->elayers[0].alayers[layer].duration = 0;

			for(k = 0
				; k < field->egroups[i].elayers[j].rp_steps
				; k++, pcmd++) {

				pcmd->id = cmd++ | CMD_ZONE_VAL(idx);
				pcmd->elevation = field->egroups[i].elayers[j].e_val;
				pcmd->azimuth = field->alayers[i].values[k];
				pcmd->rf_data.imp_count = field->egroups[i].elayers[j].values[k];
				pcmd->rf_data.imp_period = new_zone->p_period;
				pcmd->rf_data.imp_duration = new_zone->p_duration;
				pcmd->rf_data.imp_power = 100;
				pcmd->rf_data.rf_mode = 0;
				new_zone->elayers[0].alayers[layer].duration +=
						cmd_get_duration(pcmd);

			}

			new_zone->elayers[0].alayers[layer].a_range[0] =
				new_zone->elayers[0].alayers[layer].cmds[0].azimuth;

			new_zone->elayers[0].alayers[layer].a_range[1] =
				new_zone->elayers[0].alayers[layer].cmds[
				new_zone->elayers[0].alayers[layer].steps].azimuth;


			if (new_zone->elayers[0].a_range[1] < pcmd[-1].azimuth)
				new_zone->elayers[0].a_range[1] = pcmd[-1].azimuth;

			if (new_zone->elayers[0].a_range[0] >
				new_zone->elayers[0].alayers[layer].cmds->azimuth)
				new_zone->elayers[0].a_range[0] =
				new_zone->elayers[0].alayers[layer].cmds->azimuth;
		}
	}
	new_zone->elayers[0].e_range[0] =
		new_zone->elayers[0].alayers[0].e_val;

	new_zone->elayers[0].e_range[1] =
		new_zone->elayers[0].alayers[new_zone->elayers[0].alayer_cnt - 1].e_val;

	new_zone->a_range[0] = -new_zone->elayers[0].a_range[1];
	new_zone->a_range[1] = new_zone->elayers[0].a_range[1];
	new_zone->e_range[0] = new_zone->elayers[0].e_range[0];
	new_zone->e_range[1] = new_zone->elayers[0].e_range[1];

	pr_err("ERange = %d - %d (%u)\n",
			(uint32_t)(new_zone->elayers[0].e_range[0] * 1000),
			(uint32_t)(new_zone->elayers[0].e_range[1] * 1000),
			new_zone->elayers[0].alayer_cnt);
	pr_err("Arange = %d - %d\n",
			(uint32_t)(new_zone->elayers[0].a_range[0] * 1000),
			(uint32_t)(new_zone->elayers[0].a_range[1] * 1000));

	/*
	 * Create command sequence
	 */
	zs_simple_zone_prefill(new_zone, 0);

	/*
	 * Kill temporary data
	 */
	rf_data_free_t1(field);

	/*
	 * Add node to list
	 */
	list_add(&obj->node, &zs_objects);

	/*
	 * Return new node
	 */
	return &obj->zs;
err:
	/*TODO: Correct error handling needed */
	kfree(new_zone->cmds);
	kfree(new_zone);

	return NULL;
}

static void zs_simple_zone_destroy(zone_scheduler_t  *zs)
{

}

static
struct zone_schedulder_algo zs_simple = {
	.name = ZS_SIMPLE_NAME,
	.zone_probe = zs_simple_zone_probe,
	.zone_destroy = zs_simple_zone_destroy,
};

static int __init zs_init(void)
{
	int ret = 0;

	INIT_LIST_HEAD(&zs_objects);

	zs_algo_register(&zs_simple, &proc_parent);

	if (proc_parent) {
		proc_version = proc_create_data("version", 0600, proc_parent,
				&fops, &proc_file_ops[PROC_VERSION]);
		if(proc_version == NULL) {
			zs_algo_unregister(ZS_SIMPLE_NAME);
			pr_err("Out of memory here: %s:%u\n",
							__func__,
							__LINE__);
			return -ENOMEM;
		}
	}

	pr_err("ZS: Simple\n");
	return ret;
}

static void __exit zs_exit(void)
{
	zs_algo_unregister(ZS_SIMPLE_NAME);
}

module_init(zs_init);
module_exit(zs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryazantsev Vladimir <riazantsev.v@gmail.com>");
MODULE_DESCRIPTION("Zone Scheduler: Simple");

