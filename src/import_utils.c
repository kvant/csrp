#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <asm/i387.h>

#include "import_utils.h"

#define EXTRACT(dest, src, type) \
	do { \
		dest = *((type *) src); \
		src += sizeof(type); \
	} while(0)

#define EXTRACT_RAW(dest, src, sz) \
	do { \
		pr_err("Extract %lu bytes\n", sz); \
		memcpy(dest, src, sz); \
		src += sz; \
	} while(0)

int rf_data_parse_t1(rf_data_header_t *data, rf_field_t **field)
{
	int ret = -1;
	uint32_t ui32_var;
	uint32_t i, j;
	void *pos;

	if (!(data && field)) {
		pr_err("Incorrect input data. \n");
		return -1;
	}
#if 0
	if (RFDT_FIXED_TABLE != data->type) {
		pr_err("Incorrect data type = 0x%08x. \n", data->type);
		return -1;
	}
#endif
	/* Allocate header */

	if(!(*field = kzalloc(sizeof(**field), GFP_KERNEL))) {
		pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
		return -1;
	}

	/* Common data copy */
	pos = data->data;
	EXTRACT_RAW(*field, pos, offsetof(rf_field_t, alayer_cnt));

	pr_err("Pulses = %d\n", (*field)->pulse_cnt);

	/* Azimuth layers parsing */

	EXTRACT(ui32_var, pos, uint32_t);
	EXTRACT((*field)->alayer_cnt, pos, uint32_t);

	if(!(*field)->alayer_cnt) {
		pr_err("%s.%d: Azimuth layers count is ZERO! \n"
							, __func__, __LINE__);
		goto alayers_fail;
	}
	pr_err("Azimuth layers count is %d \n",(*field)->alayer_cnt);

	if(!((*field)->alayers = kzalloc(sizeof((*field)->alayers[0])
					* (*field)->alayer_cnt, GFP_KERNEL))) {
		pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
		goto alayers_fail;
	}

	for(i=0; i<(*field)->alayer_cnt; i++) {

		/* Size <not used> */
		EXTRACT(ui32_var, pos, uint32_t);

		/* Common data: index, rp_steps */
		EXTRACT_RAW(&(*field)->alayers[i], pos,
				offsetof(rf_alayer_t, values));

		if (!(*field)->alayers[i].rp_steps) {
			pr_err("Azimuth layer %d is empty \n", i);
			continue;
		}

		(*field)->alayers[i].values =
			kmalloc(sizeof((*field)->alayers[i].values[0])
					* (*field)->alayers[i].rp_steps
					, GFP_KERNEL);

		if (!(*field)->alayers[i].values) {
			pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
			/*TODO: Correct exit path */
			goto alayers_fail;
		}

		/* Values */
		EXTRACT_RAW((*field)->alayers[i].values, pos,
				sizeof((*field)->alayers[i].values[0])
					* (*field)->alayers[i].rp_steps);
	}

	/* Elevation groups parsing */
	/* Size <not used> */
	EXTRACT(ui32_var, pos, uint32_t);

	/* Count of EGs */
	EXTRACT((*field)->egroup_cnt, pos, uint32_t);

	/* Sanity */
	if (!(*field)->egroup_cnt) {
		pr_err("%s.%d: Elevation groups count is ZERO! \n"
							, __func__, __LINE__);
		goto egroups_fail;
	}

	pr_err("Groups count  = %d\n", (*field)->egroup_cnt);

	if(!((*field)->egroups = kzalloc(sizeof((*field)->egroups[0])
					* (*field)->egroup_cnt, GFP_KERNEL))) {
		pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
		goto egroups_fail;
	}

	for(i=0; i<(*field)->egroup_cnt; i++) {
		/* Size <not used> */
		EXTRACT(ui32_var, pos, uint32_t);
		/* Common data: index, elayers count */
		EXTRACT_RAW(&(*field)->egroups[i], pos,
				offsetof(rf_group_t, elayers));

		/* Sanity */
		if (!(*field)->egroups[i].elayers_cnt) {
			pr_err("Elevation group %d is empty \n", i);
			continue;
		}
		pr_err("Elayers cnt = %d\n", (*field)->egroups[i].elayers_cnt);

		(*field)->egroups[i].elayers =
			kcalloc((*field)->egroups[i].elayers_cnt
				, sizeof((*field)->egroups[i].elayers[0])
				, GFP_KERNEL);

		if (!(*field)->egroups[i].elayers) {
			pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
			/*TODO: Correct exit path */
			goto alayers_fail;
		}

		/* Extract elevation layer */
		for(j=0; j < (*field)->egroups[i].elayers_cnt; j++) {
			/* Size <not used> */
			EXTRACT(ui32_var, pos, uint32_t);

			/* Common data: index, elayers count */
			EXTRACT_RAW(&((*field)->egroups[i].elayers[j]), pos,
					offsetof(rf_elayer_t, values));

			(*field)->egroups[i].elayers[j].e_val *= (M_PI / 180.0);

			/* Sanity */
			if (!(*field)->egroups[i].elayers[j].rp_steps) {
				pr_err("Elevation layer %d in  group %d"
							" is empty \n", j, i);
				continue;
			}

			(*field)->egroups[i].elayers[j].values =
				kcalloc((*field)->egroups[i].elayers[j].rp_steps
					, sizeof((*field)->egroups[i].elayers[j].values[0])
					, GFP_KERNEL);

			if (!(*field)->egroups[i].elayers[j].values) {
				pr_err("%s.%d: Out of memory. \n", __func__, __LINE__);
				/*TODO: Correct exit path */
				goto alayers_fail;
			}

			EXTRACT_RAW((*field)->egroups[i].elayers[j].values
				, pos
				, (*field)->egroups[i].elayers[j].rp_steps
				* sizeof((*field)->egroups[i].elayers[j].values[0]));
		}

	}

return 0;

egroups_fail:

alayers_fail:
	kfree(*field);
	return ret;
}

int rf_data_free_t1(rf_field_t *field)
{
	if (!field)
		return -1;

	/* Kill azimuth layers  */

	while(field->alayer_cnt--)
		if (field->alayers[field->alayer_cnt].values)
			kfree(field->alayers[field->alayer_cnt].values);

	if(field->alayers)
		kfree(field->alayers);

	/* Kill EGroups */

	while(field->egroup_cnt--) {
		while(field->egroups[field->egroup_cnt].elayers_cnt--)
			if(field->egroups[field->egroup_cnt].elayers[field->egroups[field->egroup_cnt].elayers_cnt].values)
				kfree(field->egroups[field->egroup_cnt].elayers[field->egroups[field->egroup_cnt].elayers_cnt].values);
		kfree(field->egroups[field->egroup_cnt].elayers);
	}

	if(field->egroups)
		kfree(field->egroups);

	return 0;
}

int rf_data_dump_t1(rf_field_t *field)
{
	uint32_t i, j, k;

	if (!field)
		return -1;

	pr_err("\n\n");
	pr_err("Short name:       %s\n", field->shrt_name);
	pr_err("Full name:        %s\n", field->name);
	pr_err("Range:            %u\n", field->range_scale);
	pr_err("Probability:      %u%%\n", (uint32_t) (field->probability * 100));
	pr_err("Pulse duration:   %u\n", field->pulse_duration);
	pr_err("Pulse period:     %u\n", field->pulse_period);
	pr_err("Elayers in gr:    %u\n", field->elayer_per_gr);
	pr_err("Azimuth layers:   %u\n", field->alayer_cnt);
	pr_err("Elevation groups: %u\n", field->egroup_cnt);

	for(i=0; i < field->alayer_cnt; i++) {
		pr_err("  ALayer: %u\n", field->alayers[i].idx );
		pr_err("  Values count: %u\n", field->alayers[i].rp_steps);
		pr_err("  Values:");
		for(j=0; j < field->alayers[i].rp_steps; j++) {
			printk(" %u",(uint32_t) (field->alayers[i].values[j] * 1000) );
		}
		pr_err("\n\n\n");
	}

	for(i=0; i < field->egroup_cnt; i++) {
		pr_err("  EGroup: %d\n", field->egroups[i].idx);
		pr_err("  Elayers: %d\n", field->egroups[i].elayers_cnt);
		for(j=0; j < field->egroups[i].elayers_cnt; j++) {
			pr_err("   Layer: %d\n", field->egroups[i].elayers[j].idx);
			pr_err("   Elevation: %d\n", (int)(field->egroups[i].elayers[j].e_val * 1000));
			pr_err("   Steps: %d\n", field->egroups[i].elayers[j].rp_steps);
			pr_err("   Values:");
			for(k=0; k < field->egroups[i].elayers[j].rp_steps; k++) {
				printk(" %u", field->egroups[i].elayers[j].values[k]);
			}
			pr_err("\n\n");
		}
	}

	return 0;
}


EXPORT_SYMBOL(rf_data_parse_t1);
EXPORT_SYMBOL(rf_data_dump_t1);
EXPORT_SYMBOL(rf_data_free_t1);

