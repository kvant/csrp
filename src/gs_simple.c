/* Copyright (C)
 *
 * 2013 - Volodymyr Riazantsev <riazantsev.v@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */


#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/mempool.h>
#include <linux/seq_file.h>

#include "csrp.h"

#define MIXER_DEBUG		1

#define GS_SIMPLE_NAME		"gs_simple"
#define MAX_ZONES_NUMBER	16

#define ZRF_VALID		BIT(0)
#define ZRF_ENABLED		BIT(1)
#define ZRF_VALEN		(ZRF_VALID | ZRF_ENABLED)

#define GS_VERSION_MINOR	1
#define GS_VERSION_MAJOR	0

#define DEF_PERIOD_LEN		(30 * 1000 * 1000)	// 30 seconds
#define DEF_TIA_PERCENTS	0.40			// 40 %
#define DEF_DEFRAG_INTERVAL	2000			// 2000 usec

enum {
	SHIFT_DIFF_NEW,
	SHIFT_DIFF_CMOD,
	SHIFT_DIFF_ILEVEL,
	SHIFT_DIFF_PERIOD,
	SHIFT_DIFF_NTIME,
};

enum {
	PROC_PARENT,
	PROC_VERSION,
	PROC_PLAN_ATOMS_DUMP,
	PROC_PLAN_REQUESTS_DUMP,
	PROC_PLAN_REQUESTS_DUMP_GP,
	PROC_PLAN,
	PROC_ZONES,
	PROC_LAST,
};

static char *proc_names[] = {
	[PROC_VERSION] = "version",
	[PROC_PLAN] = "plan",
	[PROC_PLAN_ATOMS_DUMP] = "atoms",
	[PROC_PLAN_REQUESTS_DUMP] = "requests",
	[PROC_PLAN_REQUESTS_DUMP_GP] = "plan_gnuplot",
	[PROC_ZONES] = "zones",
};


#define REQ_DIFF_NEW		BIT(SHIFT_DIFF_NEW)
#define REQ_DIFF_CMDD		BIT(SHIFT_DIFF_CMOD)
#define REQ_DIFF_ILEVEL		BIT(SHIFT_DIFF_ILEVEL)
#define REQ_DIFF_PERIOD		BIT(SHIFT_DIFF_PERIOD)
#define REQ_DIFF_NTIME		BIT(SHIFT_DIFF_NTIME)

#define HEAD_POISON		0xFFBADBAD

/* Hash table size */
#define HT_SIZE			500
#define SEQ_SIZE		1024
#define CACHE_CHUNK		300

#define flags_is(zptr, fl) (((zptr)->flags & (ZRF_ ##fl)) == (ZRF_ ##fl))

#define algo_to_inst(a) container_of(a, struct gss_instance, algo)

#define pr_n_trace(...) pr_err("fn:%s ln: %d msg:%s ", __func__, __LINE__, \
				__VA_ARGS__)

static inline bool ts_before(struct timespec *tsa, struct timespec *tsb)
{
	return tsa->tv_sec ^ tsb->tv_sec ? tsa->tv_sec < tsb->tv_sec
						: tsa->tv_nsec < tsb->tv_nsec;
}

static inline bool ts_after(struct timespec *tsa, struct timespec *tsb)
{
	return tsa->tv_sec ^ tsb->tv_sec ? tsa->tv_sec > tsb->tv_sec
						: tsa->tv_nsec > tsb->tv_nsec;
}

static inline bool ts_after_or_eq(struct timespec *tsa, struct timespec *tsb)
{
	return ts_after(tsa, tsb) || timespec_equal(tsa, tsb);
}

static inline bool ts_before_or_eq(struct timespec *tsa, struct timespec *tsb)
{
	return ts_before(tsa, tsb) || timespec_equal(tsa, tsb);
}

static inline bool ts_in_range(struct timespec *arg
						, struct timespec *range_start
						, struct timespec *range_end)
{
	return ts_after_or_eq(arg, range_start)
		&& ts_before_or_eq(arg, range_end);
}

#define COL_S_PREV_E	BIT(0) /* Start time collides pevious atom end time */
#define COL_E_NEXT_S	BIT(1) /* End time collides next atom start time */
#define COL_E_NEXT_E	BIT(2) /* End time collides next atom end time */

#define set_col(val, c) val |= COL_ ## c

/* Initial request for service */

struct tia_req_record {
	unsigned cmd_time;
	unsigned self_balance;
	struct tia_req req;		/**< Copy of request */
	struct paa_cmd cmd;		/**< Copy of next command */
	struct list_head node;		/**< Connector for other items */
	struct list_head pstack;	/**< Connector for other items */
#define req_record_by_node(n) list_entry(n, struct tia_req_record, node)
#define req_record_next(rr) req_record_by_node((rr)->node.next)
#define req_record_prev(rr) req_record_by_node((rr)->node.prev)
};

/* Item of the plan */

struct tia_stack_record {
	unsigned id;
	unsigned time_deviation;
	struct timespec time_real;
	struct timespec time_planned;
	struct timespec time_planned_end;
	struct tia_req_record *record;
	struct list_head stack_node;
	struct list_head plan_node;
#define stack_rec_by_snode(node) list_entry(node, struct tia_stack_record \
						, stack_node)
#define stack_rec_by_pnode(node) list_entry(node, struct tia_stack_record \
						, plan_node)
#define stack_rec_next_by_pnode(node) list_entry((node)->plan_node.next \
						, typeof(*node), plan_node)
#define stack_rec_prev_by_pnode(node) list_entry((node)->plan_node.prev \
						, typeof(*node), plan_node)
};

/* Command group those collades or close by time */

struct tia_stack_atom {
	int min_prio;
	int max_prio;
	struct list_head  plan_list;
	struct tia_stack_record host_rec;

#define is_atom_last(atom) (atom_next(atom)->host_rec.id == HEAD_POISON)
#define is_atom_first(atom) (atom_prev(atom)->host_rec.id == HEAD_POISON)
#define atom_start_time(atom) stack_rec_by_pnode((atom)->host_rec.plan_node.next)->time_planned
#define atom_finish_time(atom) stack_rec_by_pnode((atom)->host_rec.plan_node.prev)->time_planned_end
#define atom_next(atom) list_entry((atom)->plan_list.next, typeof(*(atom)), plan_list)
#define atom_prev(atom) list_entry((atom)->plan_list.prev, typeof(*(atom)), plan_list)
};

/* Plan structure */

struct tia_plan {
	struct timespec btime;
	struct timespec etime;
	unsigned duration;
	unsigned ncmds;
	struct tia_stack_atom host_atom;
};

/* TIA zone main structure */

struct tia_zone {
	unsigned min_interval;  /**< minimal fragmentation interval, usec */
	unsigned period;	/**< Full period duration, usec  */
	unsigned quote;		/**< time allowed for TIA, usec */
	unsigned balance;	/**< Current duration, usec */
	struct tia_plan	plan;
	struct timespec time_marker;
	struct kmem_cache *request_cache;
	mempool_t *request_mempool;
	struct list_head request;
	struct list_head pstack_cache;
	struct list_head patom_cache;
};

/**
 * @brief 
 */
struct zone_rec {
	uint32_t id;
	uint32_t flags;
	struct zone_stats stats;
	struct zone_scheduler *zs;
};

/**
 * @brief 
 */
struct plan_item {
	unsigned flags;
	unsigned val[2];
	unsigned cmds_start;
	unsigned cmds_end;
	unsigned cmds_total;
	unsigned cmds_left;
	unsigned duration;
	unsigned tte;
	struct zone_rec *zone;
	struct plan_item *next;
};

struct zone_range {
	unsigned id;
	unsigned range;
	struct list_head node;
};

/**
 * @brief
 */
struct gss_instance {
	unsigned            period;		/**< Period duraton in usec */
	unsigned            duration;		/**< Duration of TWS mode, usec, < period */
	unsigned            cmds_total;		/**< Total TWS commands number */
	unsigned            cmds_rem;		/**< Remaining commands number */
	unsigned            tte;		/**< TimeToEnd, TWS, usec */
	unsigned            num_pitems;		/**< Number of Plan items */
	struct timespec     period_begin;	/**< Period start time */
	struct timespec     period_eng;		/**< Period finish time */
	unsigned            time_pointer;	/**< Time gone from period begin */
	struct global_schedulder_algo algo; 	/**< Internal Algo instance */
	struct tia_zone     tia_zone;		/**< TIA zone instance */
	struct plan_item    *plan;		/**< Plan Items */
	struct plan_item    *curr;		/**< Current plan item */
	struct zone_rec     zones[MAX_ZONES_NUMBER];	/**< Zones instances */
	struct list_head    zones_by_range;	/**< Some helper */
	spinlock_t          lock;		/**< Internal spinlock */
	struct {
		struct proc_dir_entry *entry;
		struct proc_dir_entry *parent;
		}           proc_entries[PROC_LAST];
};

static struct gss_instance instance;

enum sr_insert_t {
	SR_INSERT_TAIL,
	SR_INSERT_HEAD,
	SR_INSERT_ADAPTIVE,
	SR_INSERT_ADAPTIVE_GROW_RIGHT,
	SR_INSERT_ADAPTIVE_GROW_LEFT,
};

typedef enum {
	ATOM_JOIN_FORWARD,
	ATOM_JOIN_BACKWARD,
	ATOM_JOIN_COMPROMISE,
} join_mode_t;

/* time till first command */
/**
 * @brief 
 *
 * @param zone
 *
 * @return 
 */
#if 0
static unsigned gss_tia_get_ttf(struct tia_zone *zone)
{

	return 0;
}
#endif
static inline void atom_get_room(struct tia_stack_atom *__restrict__ sa
						, unsigned *__restrict__ head
						, unsigned *__restrict__ tail);

static int gss_get_cmds_timed(struct global_schedulder_algo *algo
						, struct paa_cmd_array *cmds
						, unsigned max_cnt
						, unsigned *duration_us
						, unsigned *next_duration);

void dump_ts(const char *str, struct timespec ts)
{
	pr_err("%s %ld.%ld \n", str, ts.tv_sec, ts.tv_nsec);
}

void dump_record(struct tia_stack_record *sr, int count, struct seq_file *s)
{
	seq_printf(s, "   [R-%d]  Id:%04d P:%02d RT:%ld.%ld PT:%ld.%ld ET:%ld.%ld DEV:%d LEN:%u\n"
				, count
				, sr->record->req.id
				, sr->record->req.ilevel
				, sr->time_real.tv_sec
				, nsec_to_usec(sr->time_real.tv_nsec)
				, sr->time_planned.tv_sec
				, nsec_to_usec(sr->time_planned.tv_nsec)
				, sr->time_planned_end.tv_sec
				, nsec_to_usec(sr->time_planned_end.tv_nsec)
				, sr->time_deviation
				, sr->record->cmd_time);
}

void dump_rr(struct tia_req_record *rr, struct seq_file *s)
{
	struct tia_stack_record *sr;
	unsigned count = 0;

	seq_printf(s, "  [Id-%u] NT:%ld.%ld PERIOD:%u PRIO:%d BALANCE:%u \n"
			, rr->req.id
			, rr->req.next_time.tv_sec
			, rr->req.next_time.tv_nsec / 1000
			, rr->req.period
			, rr->req.ilevel
			, rr->self_balance);

	list_for_each_entry(sr, &rr->pstack, stack_node) {
		dump_record(sr, count++, s);
	}
}

void dump_requests(struct tia_zone *zone, struct seq_file *s)
{
	struct tia_req_record *rr;

	seq_printf(s, " REQUESTS: \n ");

	list_for_each_entry(rr, &zone->request, node) {
		dump_rr(rr, s);
	}
}

#define RECT_HEIGHT	250

void dump_requests_gp(struct tia_zone *zone, struct seq_file *s)
{
	struct tia_stack_record *sr;
	struct tia_stack_atom *sa;
	struct timespec plan_start = {10, 0};
	unsigned count = 1;
	int x1, x2, y1, y2;
	unsigned sr_id;

int map_rec_id(unsigned id, bool init) {
	static unsigned m_id[100];
	unsigned i = 0;

	if (init) {
		for (; i < ARRAY_SIZE(m_id); m_id[i++] = 0);
		return 0;
	}

	for (; i < ARRAY_SIZE(m_id) && m_id[i] && m_id[i] != id; i++);

	if (i == ARRAY_SIZE(m_id))
		return -1;

	if (!m_id[i])
		m_id[i] = id;

	return i;
}
	map_rec_id(0, 1);

	list_for_each_entry(sa, &zone->plan.host_atom.plan_list, plan_list) {
		list_for_each_entry(sr, &sa->host_rec.plan_node, plan_node) {

			struct timespec tv = sr->time_real;

			sr_id = map_rec_id(sr->record->req.id, 0);
			sr_id++;

			tv = timespec_sub(tv, plan_start);

			x1 = ts_to_usec(&tv);
			x2 = x1 + sr->record->cmd_time;
			y1 = sr_id * RECT_HEIGHT;
			y2 = y1 + RECT_HEIGHT;

			seq_printf(s,   "set object %d rectangle from %d, %d to %d, %d "
					"fillcolor palette frac 0.00 fillstyle solid 0.8\n"
					,count++
					,x1, y1
					,x2, y2);

			tv = sr->time_planned;
			tv = timespec_sub(tv, plan_start);

			x1 = ts_to_usec(&tv);
			x2 = x1 + sr->record->cmd_time;
			y1 = 0;
			y2 = RECT_HEIGHT;

			seq_printf(s,   "set object %d rectangle from %d, %d to %d, %d "
					"fillcolor palette frac 0.00 fillstyle solid 0.8\n"
					,count++
					,x1, y1
					,x2, y2);

		}
	}
}

void dump_atom(struct tia_stack_atom *sa, int id, struct seq_file *s)
{
	struct timespec ts_st = atom_start_time(sa);
	struct timespec ts_et = atom_finish_time(sa);
	struct tia_stack_record *sr;
	unsigned room_head, room_tail;
	int count = 0;

	atom_get_room(sa, &room_head, &room_tail);

	seq_printf(s, "\n  [A-%d]  MinPrio:%d MaxPrio:%d STime:%ld.%ld ETime:%ld.%ld ROOM[%u : %u]\n"
			, id
			, sa->min_prio
			, sa->max_prio
			, ts_st.tv_sec, nsec_to_usec(ts_st.tv_nsec)
			, ts_et.tv_sec, nsec_to_usec(ts_et.tv_nsec)
			, room_head
			, room_tail);

	list_for_each_entry(sr, &sa->host_rec.plan_node, plan_node) {
		dump_record(sr, count++, s);
	}
}

void dump_plan(struct tia_zone *zone, int flags, struct seq_file *s)
{
	struct tia_stack_atom *sa;
	int count = 0;
	seq_printf(s, "Plan:\n");
	seq_printf(s, " BTime: %ld.%ld \n", zone->plan.btime.tv_sec, nsec_to_usec(zone->plan.btime.tv_nsec));
	seq_printf(s, " ETime: %ld.%ld \n", zone->plan.etime.tv_sec, nsec_to_usec(zone->plan.etime.tv_nsec));
	seq_printf(s, " Duration: %d \n", zone->plan.duration);
	seq_printf(s, " Balance: %d \n\n", zone->balance);


	list_for_each_entry(sa, &zone->plan.host_atom.plan_list, plan_list) {
		dump_atom(sa, count++, s);
	}
}

static inline int sr_calc_time_deviation(struct tia_stack_record *sr
						, struct timespec *pivot_time
						, bool pivot_is_finish_time)
{
	struct timespec start = *pivot_time;

	if (pivot_is_finish_time)
		ts_sub_usec(&start, sr->record->cmd_time);

	if (ts_after(&start, &sr->time_real)) {
		start = timespec_sub(start, sr->time_real);
		return ts_to_usec(&start);
	}

	start = timespec_sub(sr->time_real, start);
	return -ts_to_usec(&start);
}

#define timespec_sub_abs(a, b) (ts_after(&(a), &(b)) ? timespec_sub(a, b) \
						: timespec_sub(b, a) )


/**
 * @brief 
 *
 * @param tz
 *
 * @return 
 */
static inline
struct tia_stack_atom *gss_tia_get_atom_record(struct tia_zone *tz)
{
	struct tia_stack_atom *res;

	if (unlikely(list_empty(&tz->patom_cache))) {
		unsigned i = 0;
		struct tia_stack_atom *arr = kzalloc(CACHE_CHUNK * sizeof *arr
							, GFP_KERNEL);
		if (!arr) {
			pr_err("Can't allocate atom cache !");
			BUG();
		}

		for (; i < CACHE_CHUNK; i++) {
			INIT_LIST_HEAD(&arr[i].plan_list);
			INIT_LIST_HEAD(&arr[i].host_rec.stack_node);
			INIT_LIST_HEAD(&arr[i].host_rec.plan_node);
			arr[i].host_rec.time_planned.tv_sec = ~0UL >> 1;
			arr[i].host_rec.time_planned.tv_nsec = ~0UL >> 1;
			arr[i].host_rec.time_planned_end.tv_sec = 0;
			arr[i].host_rec.time_planned_end.tv_nsec = 0;
			list_add(&arr[i].plan_list, &tz->patom_cache);
		}
	}

	res = list_entry(tz->patom_cache.next, struct tia_stack_atom
								, plan_list);

	list_del(tz->patom_cache.next);

	return res;
}

/**
 * @brief 
 *
 * @param 
 * @param atom
 */
static inline
void gss_tia_put_atom_record(struct tia_zone *tz
						, struct tia_stack_atom *atom)
{
	list_move(&atom->plan_list, &tz->patom_cache);
}

/**
 * @brief 
 *
 * @param tz
 *
 * @return 
 */
static inline
struct tia_stack_record *gss_tia_get_stack_record(struct tia_zone *tz)
{
	struct tia_stack_record *res;

	if (unlikely(list_empty(&tz->pstack_cache))) {
		unsigned i = 0;
		struct tia_stack_record *arr = kzalloc(CACHE_CHUNK * sizeof *arr
							, GFP_KERNEL);
		if (!arr) {
			pr_err("Can't allocate records cache !");
			BUG();
		}

		for (; i < CACHE_CHUNK; i++) {
			INIT_LIST_HEAD(&arr[i].stack_node);
			INIT_LIST_HEAD(&arr[i].plan_node);
			list_add(&arr[i].stack_node, &tz->pstack_cache);
		}
	}

	res = stack_rec_by_snode(tz->pstack_cache.next);
	list_del(tz->pstack_cache.next);

	return res;
}

/**
 * @brief 
 *
 * @param 
 * @param rec
 */
static inline
void gss_tia_put_stack_record(struct tia_zone *tz
					, struct tia_stack_record *rec)
{
	list_del(&rec->plan_node);
	list_move(&rec->stack_node, &tz->pstack_cache);
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param stime
 *
 * @return 
 */
static inline struct tia_stack_atom *atom_new_by_rrecord(struct tia_zone *tz
						, struct tia_req_record *rr
						, struct timespec *stime)
{
	struct tia_stack_record *sr = gss_tia_get_stack_record(tz);
	struct tia_stack_atom *sa = gss_tia_get_atom_record(tz);

	if (!(sa && sr)) {
		pr_err("Something terrible here!\n");
		BUG();
	}

	sr->time_deviation = 0;
	sr->time_real = sr->time_planned = sr->time_planned_end = *stime;
	sr->record = rr;

	ts_add_usec(&sr->time_planned_end, rr->cmd_time);

	/* Add to record stack */
	list_add_tail(&sr->stack_node, &rr->pstack);

	/* Add to atom list*/
	list_add_tail(&sr->plan_node, &sa->host_rec.plan_node);

	sa->min_prio = sa->max_prio = rr->req.ilevel;

	return sa;
}

/**
 * @brief 
 *
 * @param satom
 */
static inline void atom_destroy(struct tia_zone *tz, struct tia_stack_atom *sa)
{
	struct tia_stack_record *sr;

	while (!list_empty(&sa->host_rec.plan_node)) {
		sr = list_entry(sa->host_rec.plan_node.prev
						, typeof(*sr)
						, plan_node);

		gss_tia_put_stack_record(tz, sr);
	}

	/* Remove from plan */
	gss_tia_put_atom_record(tz, sa);
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param tail
 */
static inline void atom_get_room(struct tia_stack_atom *__restrict__ sa
						, unsigned *__restrict__ head
						, unsigned *__restrict__ tail)
{
	struct timespec ts_head = atom_start_time(sa);

	struct timespec ts_tail = atom_start_time((list_entry(sa->plan_list.next
						, typeof(*sa)
						, plan_list)));
	if (unlikely(is_atom_first(sa))) {
		ts_head.tv_sec = ~0UL >> 1;
		ts_head.tv_sec = ~0UL >> 1;
	} else {
		ts_head = timespec_sub(ts_head
					, atom_finish_time((list_entry(sa->plan_list.prev
					, typeof(*sa)
					, plan_list))));
	}

	if (unlikely(is_atom_last(sa))) {
		ts_tail.tv_sec = ~0UL >> 1;
		ts_tail.tv_sec = ~0UL >> 1;
	} else {
		ts_tail = timespec_sub(ts_tail, atom_finish_time(sa));
	}

	*head = ts_to_usec(&ts_head);
	*tail = ts_to_usec(&ts_tail);
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param 
 * @param forward
 */
static inline void atom_move_plan(struct tia_stack_atom *sa
					, struct tia_stack_record *sr
					, struct timespec *tline
					, bool forward)
{
	typeof(sr) sr_i = sr;
	struct timespec dt;

	if (forward) {
		while (sr_i != &sa->host_rec) {
			sr_i->time_planned = sr_i->time_planned_end = *tline;
			ts_sub_usec(&sr_i->time_planned, sr_i->record->cmd_time);
			*tline = sr_i->time_planned;
			dt = sr_i->time_planned;
			dt = timespec_sub(dt, sr_i->time_real);
			sr_i->time_deviation = ts_to_usec(&dt);
			sr_i = list_entry(sr_i->plan_node.prev
						, typeof(*sr_i)
						, plan_node);
		}
	} else {
		while (sr_i != &sa->host_rec) {
			sr_i->time_planned = sr_i->time_planned_end = *tline;
			ts_add_usec(&sr_i->time_planned_end, sr_i->record->cmd_time);
			*tline = sr_i->time_planned_end;
			dt = sr_i->time_planned;
			dt = timespec_sub(dt, sr_i->time_real);
			sr_i->time_deviation = ts_to_usec(&dt);
			sr_i = list_entry(sr_i->plan_node.next
						  , typeof(*sr_i)
						  , plan_node);
		}
	}
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param forward
 */
static inline void atom_update_plan_times_single(struct tia_stack_atom *sa
					, struct tia_stack_record *pivot
					, bool forward)
{
	struct timespec pivot_time = forward
					? pivot->time_planned_end
					: pivot->time_planned;

	atom_move_plan(sa, pivot, &pivot_time, forward);
}

static inline void atom_update_plan_times(struct tia_stack_atom *sa
					, struct tia_stack_record *pivot)
{
	struct timespec t = pivot->time_planned;
	atom_move_plan(sa, pivot, &t, false);

	t = pivot->time_planned_end;
	atom_move_plan(sa, pivot, &t, true);
}

static inline void atom_shift_plan(struct tia_stack_atom *sa
					, struct tia_stack_record *sr
					, unsigned stime
					, bool forward)
{
	typeof(sr) sr_i = sr;
	struct timespec dt;

	if (forward) {
		while (sr_i != &sa->host_rec) {
			ts_sub_usec(&sr_i->time_planned, stime);
			ts_sub_usec(&sr_i->time_planned_end, stime);
			dt = sr_i->time_planned;
			dt = timespec_sub(dt, sr_i->time_real);
			sr_i->time_deviation = ts_to_usec(&dt);
			sr_i = list_entry(sr_i->plan_node.prev
						, typeof(*sr_i)
						, plan_node);
		}
	} else {
		while (sr_i != &sa->host_rec) {
			ts_add_usec(&sr_i->time_planned, stime);
			ts_add_usec(&sr_i->time_planned_end, stime);
			dt = sr_i->time_planned;
			dt = timespec_sub(dt, sr_i->time_real);
			sr_i->time_deviation = ts_to_usec(&dt);
			sr_i = list_entry(sr_i->plan_node.next
						  , typeof(*sr_i)
						  , plan_node);
		}
	}
}

static inline
struct tia_stack_atom *atom_join(struct tia_stack_atom *a
						, struct tia_stack_atom *b
						, join_mode_t mode)
{
	int dt_int;
	bool forward;
	struct timespec dt_ts;
	struct tia_stack_record *a_last = stack_rec_prev_by_pnode(&a->host_rec);
	struct tia_stack_record *b_frst = stack_rec_next_by_pnode(&b->host_rec);

	mode = a->max_prio < b->max_prio ? ATOM_JOIN_FORWARD
		: (a->max_prio > b->max_prio ? ATOM_JOIN_BACKWARD : ATOM_JOIN_COMPROMISE);

	while (!list_empty(&b->host_rec.plan_node)) {
		struct tia_stack_record *sr = stack_rec_by_pnode(b->host_rec.plan_node.next);
		list_move(b->host_rec.plan_node.next
				, a->host_rec.plan_node.prev);
		if (sr->record->req.ilevel > a->max_prio)
			a->max_prio = sr->record->req.ilevel;
		else if (sr->record->req.ilevel < a->min_prio)
			a->min_prio = sr->record->req.ilevel;
	}

	switch (mode) {

	case ATOM_JOIN_COMPROMISE:

		/* Calculate offset for both atoms */
		dt_ts = timespec_sub_abs(a_last->time_planned_end
						, b_frst->time_planned);
		dt_int = ts_to_usec(&dt_ts);
		dt_int >>= 1;

		if (ts_before(&a_last->time_planned_end, &b_frst->time_planned)) {
			ts_add_usec(&a_last->time_planned, dt_int);
		} else {
			ts_sub_usec(&a_last->time_planned, dt_int);
		}

		a_last->time_planned_end = a_last->time_planned;
		ts_add_usec(&a_last->time_planned_end, a_last->record->cmd_time);

		atom_update_plan_times(a, a_last);
		goto exit;
	break;

	case ATOM_JOIN_FORWARD:
		if (ts_before(&a_last->time_planned_end, &b_frst->time_planned)) {
			forward = false;
		} else {
			a_last = b_frst;
			forward = true;
		}
	break;

	case ATOM_JOIN_BACKWARD:
		if (ts_before(&a_last->time_planned_end, &b_frst->time_planned)) {
			a_last = b_frst;
			forward = true;
		} else {
			forward = false;
		}
	break;
	}

	atom_update_plan_times_single(a, a_last, forward);

exit:
	return a;
}

static int tia_plan_defrag(struct tia_zone *tz, unsigned max_interval)
{
	bool joined = false;
	struct tia_stack_atom *atom;

	do {
		joined = false;
		atom = atom_next(&tz->plan.host_atom);

		while(atom_next(atom) != &tz->plan.host_atom) {
			struct timespec dt_ts = atom_start_time(atom_next(atom));
			ts_sub_usec(&dt_ts, max_interval);
			if (ts_after(&atom_finish_time(atom), &dt_ts)) {
				atom_join(atom, atom_next(atom), ATOM_JOIN_COMPROMISE);
				joined = true;
				atom_destroy(tz, atom_next(atom));
				break;
			}
			atom = atom_next(atom);
		}
	} while (joined);

	return 0;
}

static inline
struct tia_stack_record *atom_search_possible_place(struct tia_stack_atom *sa
						, struct tia_req_record *rr
						, struct timespec *stime
						, unsigned *offset
						, bool reverse)
{
	struct tia_stack_record    *sr = NULL
				, *srr = NULL
				, *res = NULL;
	struct timespec dt_head;
	struct timespec dt_tail;

	/* TODO: Currently there is no rule to do this */

	if (rr->req.ilevel < sa->max_prio || rr->req.ilevel > sa->min_prio) {

		if (offset)
			*offset = ~0U;

		pr_n_trace("!!! ERROR case here !\n");

		return NULL;
	}

	if (reverse) {
		list_for_each_entry_reverse(srr, &sa->host_rec.plan_node, plan_node) {

			pr_err(" PRIO: %d<->%d", srr->record->req.ilevel, rr->req.ilevel);
			if (srr->record->req.ilevel <= rr->req.ilevel) {
				dt_head = srr->time_planned_end;
				res = srr;
				break;
			}

			if (ts_in_range(stime
					, &srr->time_planned
					, &srr->time_planned_end)) {

				/*
				 * This is an optimal case
				 */

				dt_head = timespec_sub(*stime, res->time_planned);
				dt_tail = timespec_sub(res->time_planned_end, *stime);

				dump_ts(">> HEAD:", dt_head);
				dump_ts(">> TAIL:", dt_tail);

				if (offset)
					*offset = 0U;

				return ts_after(&dt_head, &dt_tail)
					? srr
					: stack_rec_prev_by_pnode(srr);
			}

		}
	} else {
		list_for_each_entry(sr, &sa->host_rec.plan_node, plan_node) {

			if (sr->record->req.ilevel <= rr->req.ilevel) {
				dt_head = sr->time_planned;
				ts_sub_usec(&dt_head, rr->cmd_time);
				res = sr;
				break;
			}

			if (ts_in_range(stime
					, &sr->time_planned
					, &sr->time_planned_end)) {

				/*
				 * This is an optimal case
				 */

				dt_head = sr->time_planned;
				ts_sub_usec(&dt_head, rr->cmd_time);
				dt_head = timespec_sub_abs(dt_head, *stime);

				dt_tail = sr->time_planned_end;
				ts_sub_usec(&dt_tail, rr->cmd_time);
				dt_tail = timespec_sub_abs(dt_tail, *stime);

				if (offset)
					*offset = 0U;

				dump_ts(">> HEAD:", dt_head);
				dump_ts(">> TAIL:", dt_tail);

				return  ts_after(&dt_head, &dt_tail)
					? stack_rec_next_by_pnode(sr)
					: sr;
			}
		}
	}

	if (offset) {
		dt_tail = timespec_sub_abs(dt_head, *stime);
		*offset = ts_to_usec(&dt_tail);
	}

	return res;
}
/**
 * @brief 
 *
 * @param 
 * @param mode
 */
static inline int atom_add_srecord(struct tia_zone *tz
						, struct tia_stack_atom *sa
						, struct tia_req_record *rr
						, struct timespec *stime
						, enum sr_insert_t mode)
{
	struct timespec head_dt;
	struct timespec tail_dt;
	struct tia_stack_record *it/*, *it1*/;
	struct tia_stack_record *sr = gss_tia_get_stack_record(tz);
	unsigned room_tail;
	unsigned room_head;

	if (!sr) {
		/* TODO: */
		BUG();
	}

	/* TODO: Separate routine ? */
	sr->id = 0;
	sr->time_deviation = 0;
	sr->record = rr;
	sr->time_real = *stime;

	atom_get_room(sa, &room_head, &room_tail);

	if (room_head < rr->cmd_time && room_tail <= room_head) {
		pr_err("%s:%d: Can't insert cmd to tail beause of lack room\n"
								, __func__
								, __LINE__);
		gss_tia_put_stack_record(tz, sr);
		return -EINVAL;
	}

	list_add_tail(&sr->stack_node, &rr->pstack);

	switch (mode) {
	case SR_INSERT_TAIL:
insert_tail:
		pr_err(" >> TAIL \n");
		if (room_tail < rr->cmd_time) {
			pr_err("Can't insert cmd to tail beause of lack room\n");
			return -EINVAL;
		}

		/* Sanity check, seems not needed */
		if (unlikely(list_empty(&sa->host_rec.plan_node)))
			sr->time_planned = sr->time_planned_end = *stime;
		 else
			sr->time_planned = sr->time_planned_end = atom_finish_time(sa);

		ts_add_usec(&sr->time_planned_end, rr->cmd_time);
		list_add_tail(&sr->plan_node, &sa->host_rec.plan_node);

		break;

	case SR_INSERT_HEAD:
insert_head:
		pr_err(" >> HEAD \n");
		if (room_head < rr->cmd_time) {
			pr_err("Can't insert cmd to tail beause of lack room\n");
			return -EINVAL;
		}

		/* Sanity check, seems not needed */
		if (unlikely(list_empty(&sa->host_rec.plan_node)))
			sr->time_planned = sr->time_planned_end = *stime;
		 else
			sr->time_planned = sr->time_planned_end = atom_start_time(sa);

		ts_sub_usec(&sr->time_planned, rr->cmd_time);
		list_add(&sr->plan_node, &sa->host_rec.plan_node);

		break;

	case SR_INSERT_ADAPTIVE:
		pr_err(" >> ADAPTIVE \n");
		if (rr->req.ilevel >= sa->min_prio) {

			/*
			 * If priority is less than minimal
			 * We can insert in head or tail
			 */

			tail_dt = atom_finish_time(sa);
			head_dt = atom_start_time(sa);
			ts_sub_usec(&head_dt, rr->cmd_time);

			tail_dt = timespec_sub_abs(*stime, tail_dt);
			head_dt = timespec_sub_abs(*stime, head_dt);

			if (ts_after(&tail_dt, &head_dt)) {
				if (room_head >= rr->cmd_time)
						goto insert_head;
				else
						goto insert_tail;
			} else {
				if (room_tail >= rr->cmd_time)
						goto insert_tail;
				else
						goto insert_head;
			}
		} else if (rr->req.ilevel < sa->max_prio) {

			/*
			 * This is will be a new pivot item within atom
			 * Calculating times
			 */

			sr->time_planned = sr->time_planned_end = *stime;
			ts_add_usec(&sr->time_planned_end, rr->cmd_time);

			/*
			 * If priority is more than max
			 */

			list_for_each_entry(it, &sa->host_rec.plan_node, plan_node)
				if (ts_in_range(&sr->time_planned
						, &it->time_planned
						, &it->time_planned_end) ||
				ts_in_range(&sr->time_planned_end
						, &it->time_planned
						, &it->time_planned_end) )
					break;

			/* Check ways to move other items */

			head_dt = timespec_sub_abs(sr->time_planned
							, it->time_planned_end);
			tail_dt = timespec_sub_abs(sr->time_planned_end
							, it->time_planned);

			if (ts_after(&head_dt, &tail_dt)) {
				if ( room_tail > ts_to_usec(&tail_dt))
					/* Move plan "right" */
					list_add_tail(&sr->plan_node, &it->plan_node);
				else
					/* Move plan "left" */
					list_add(&sr->plan_node, &it->plan_node);
			} else {
				if ( room_head > ts_to_usec(&head_dt))
					/* Move plan "left" */
					list_add(&sr->plan_node, &it->plan_node);
				else
					/* Move plan "right" */
					list_add_tail(&sr->plan_node, &it->plan_node);
			}

			atom_update_plan_times(sa, sr);
		} else {
			unsigned head_dt = ~0U >> 1;
			unsigned tail_dt = ~0U >> 1;
			struct tia_stack_record *sr_head = NULL,
						*sr_tail = NULL;

			/* All others. Try to find optimal position */

			if (room_head >= rr->cmd_time) {
				sr_head = atom_search_possible_place(sa, rr
								, stime
								, &head_dt
								, false);
				if (! head_dt)
					/* This is an optimal case */
					goto skip_reverse_check;

			}

			if (room_tail >= rr->cmd_time)
				 sr_tail = atom_search_possible_place(sa, rr
						 		, stime
								, &tail_dt
								, true);

skip_reverse_check:
			if (head_dt < tail_dt) {
				list_add_tail(&sr->plan_node, &sr_head->plan_node);
				sr->time_planned_end = sr_head->time_planned;
				atom_update_plan_times_single(sa, sr, true);
			} else {
				list_add(&sr->plan_node, &sr_tail->plan_node);
				sr->time_planned = sr_tail->time_planned_end;
				atom_update_plan_times_single(sa, sr, false);
			}
		}

		break;

	case SR_INSERT_ADAPTIVE_GROW_RIGHT:
	case SR_INSERT_ADAPTIVE_GROW_LEFT:

		pr_err("Insert mode %d doesn't implemented\n", mode);
		return -EINVAL;

	default:

		pr_err("Unknown insert mode\n");
		return -EINVAL;
	}

	/* Common updates */

	if (rr->req.ilevel < sa->max_prio)
		sa->max_prio = rr->req.ilevel;
	else if (rr->req.ilevel > sa->min_prio)
		sa->min_prio = rr->req.ilevel;

	head_dt = sr->time_planned;
	head_dt = timespec_sub(head_dt, sr->time_real);
	sr->time_deviation = ts_to_usec(&head_dt);

	tz->plan.duration += rr->cmd_time;

	return 0;
}

static inline bool is_prio_more(struct tia_req_record *a, struct tia_req_record *b)
{
	return a->req.ilevel < b->req.ilevel;
}

/**
 * @brief
 *
 * @param
 * @param req
 *
 * @return
 */

static inline struct tia_req_record *gss_tia_find_req(struct tia_zone *zone
							, struct tia_req *req)
{
	struct tia_req_record *record;

	list_for_each_entry(record, &zone->request, node) {
		if (req->id == record->req.id)
			return record;
	}

	return NULL;
}

static inline unsigned tia_req_record_calc_balance(struct tia_zone *zone
						, struct tia_req_record *rr)
{
	unsigned cd = cmd_get_duration(&rr->cmd);

	return  ((zone->period - cd) / rr->req.period) * cd;
}

static inline void gss_tia_insert_rr(struct tia_zone *zone
						, struct tia_req_record *rr)
{
	struct tia_req_record *record;

	if (is_prio_more(rr, req_record_by_node(zone->request.next))) {
		list_add(&rr->node, &zone->request);
	} else if (is_prio_more(req_record_by_node(zone->request.prev), rr)) {
		list_add_tail(&rr->node, &zone->request);
	} else {
		list_for_each_entry(record, &zone->request, node) {
			if (is_prio_more(rr, record)) {
				list_add_tail(&rr->node, &record->node);
				break;
			}
		}
	}
}

#if 0
static void gss_tia_cut_plan(struct gss_instance *instance, unsigned btime
						, bool tail)
{

}
#endif

static void gss_tia_flush_plan(struct gss_instance *instance)
{
	while (!list_empty(&instance->tia_zone.plan.host_atom.plan_list)) {
		atom_destroy(&instance->tia_zone
			, atom_next(&instance->tia_zone.plan.host_atom));
	}
}

static void gss_tia_update_plan(struct gss_instance *instance)
{
	unsigned collision = 0;
	unsigned attempt = 0;

	struct tia_stack_atom *atom;
	struct tia_zone *tz = &instance->tia_zone;

	struct timespec stime = tz->plan.btime;
	struct timespec ftime = tz->plan.etime;
	struct timespec etime;

	struct list_head *pos;
	struct tia_stack_record *stack;

	/* List already ordered by priority level */

	list_for_each(pos, &tz->request) {

		int64_t dt, ndt;
		struct tia_req_record *rec = req_record_by_node(pos);

		if (list_empty(&rec->pstack)) {
			struct timespec ts_dt = timespec_sub(tz->plan.btime
							, rec->req.next_time);

			if (ts_before(&tz->plan.btime, &rec->req.next_time)) {
				ts_dt = timespec_sub(rec->req.next_time, tz->plan.btime);
				dt = ts_to_usec(&ts_dt);
			} else {
				ts_dt = timespec_sub(tz->plan.btime, rec->req.next_time);
				dt = ts_to_usec(&ts_dt);
				dt = -(dt % rec->req.period);
				dt += dt ? rec->req.period : 0;
			}
		} else {
			struct timespec ts_dt;
			stack = stack_rec_by_snode(rec->pstack.prev);
			ts_dt = stack->time_real;

			if (ts_before(&ts_dt, &tz->plan.btime)) {
				ts_dt = timespec_sub(tz->plan.btime, ts_dt);
				dt = ts_to_usec(&ts_dt);
				dt = -dt;
				dt += rec->req.period;
			} else {
				ts_dt = timespec_sub(ts_dt, tz->plan.btime);
				dt = ts_to_usec(&ts_dt);
				dt += rec->req.period;
			}
		}

		ndt = dt;
		stime = tz->plan.btime;

		if (ndt > 0)
			ts_add_usec(&stime, ndt);
		else
			ts_sub_usec(&stime, ndt);

		etime = stime;
		ts_add_usec(&etime, rec->cmd_time);

		while (ts_before(&stime, &ftime)) {

			int res = 0;
			collision = 0;

			/*
			 * Find an rigth place to insert
			 */

			for (atom = atom_next(&tz->plan.host_atom);
				atom != &tz->plan.host_atom &&
					ts_after(&stime, &(atom_start_time(atom)));
				atom = atom_next(atom)
			);

			/*
			 * Check for collisions
			 * TODO: Seems not a best option ?
			 */

			if (ts_after(&atom_finish_time(atom_prev(atom)), &stime)) {
				pr_err("Collision: S_PREV_E\n");
				set_col(collision, S_PREV_E);
			}

			if (ts_after(&etime, &atom_start_time(atom))) {
				pr_err("Collision:  E_NEXT_S\n");
				set_col(collision, E_NEXT_S);

				if (ts_after(&etime, &atom_finish_time(atom))) {
					pr_err("Collision:  E_NEXT_E\n");
					set_col(collision, E_NEXT_E);
				}
			}

			if (!collision) {
				/*
				 * No collisions - create new atom
				 */

				struct tia_stack_atom *new_atom;

				if (!(new_atom = atom_new_by_rrecord(tz, rec, &stime))) {
					BUG();
				}
				list_add_tail(&new_atom->plan_list, &atom->plan_list);
				res = 0;

			} else if (collision & COL_S_PREV_E) {

				res = atom_add_srecord(tz, atom_prev(atom), rec, &stime, SR_INSERT_ADAPTIVE);

			} else if (collision & COL_E_NEXT_S) {

				res = atom_add_srecord(tz, atom, rec, &stime, SR_INSERT_ADAPTIVE);

			}

			if (res) {
				/*
				 * Probably there is no room to insert
				 * Early defragmentation
				 */
				if (!attempt) {
					pr_warn(">> Can't insert command, try to early defrag.\n");
					tia_plan_defrag(tz, rec->cmd_time);
					++attempt;
					continue;
				} else {
					pr_err(">> !! Can't insert commant on 2nd attempt \n");
				}
			}

			attempt = 0;
			ts_add_usec(&stime, rec->req.period);
			ts_add_usec(&etime, rec->req.period);
		}
	}

	tia_plan_defrag(tz, instance->tia_zone.min_interval);
}

static void gss_tia_create_plan(struct gss_instance *instance
						, struct timespec *btime
						, unsigned duration
						, bool flush)
{
	instance->tia_zone.plan.btime = instance->tia_zone.plan.etime = *btime;
	instance->tia_zone.plan.duration = duration;
	ts_add_usec(&instance->tia_zone.plan.etime, duration);

	if (flush)
		gss_tia_flush_plan(instance);

	gss_tia_update_plan(instance);
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param flush
 */
#if 0
static void gss_tia_move_plan_window(struct gss_instance *instance
						, unsigned dtime
						, bool flush)
{
	ts_add_usec(&instance->tia_zone.plan.btime, dtime);

	gss_tia_create_plan(instance, &instance->tia_zone.plan.btime
				, instance->tia_zone.plan.duration, flush);
}
#endif
/**
 * @brief 
 *
 * @param 
 * @param 
 * @param mask
 *
 * @return 
 */
static inline unsigned gss_get_request_diff(struct tia_req *new
						, struct tia_req *old
						, unsigned mask)
{
	unsigned res = 0x0;

	res = ((new->ilevel != old->ilevel) << SHIFT_DIFF_ILEVEL) |
		((new->period != old->period) << SHIFT_DIFF_PERIOD) |
		((! timespec_equal(&new->next_time, &old->next_time)) << SHIFT_DIFF_NTIME);

	return res & mask;
}

/**
 * @brief 
 *
 * @param 
 * @param cmd
 *
 * @return 
 */
static inline int gss_get_cmd_by_request(struct gss_instance *instance
							,struct tia_req *req
							, struct paa_cmd *cmd)
{
	int res = -EINVAL;
	unsigned range = req->d;
	struct zone_range *zrange;

	list_for_each_entry(zrange, &instance->zones_by_range, node) {
		struct zone_stats *stats;


		if (zrange->range < range)
			continue;

		stats = &instance->zones[zrange->id].stats;

		if ( stats->e_range[0] <= req->e &&
			stats->e_range[1] >= req->e &&
			stats->a_range[0] <= req->a &&
			stats->a_range[1] >= req->a ) {

			res = instance->zones[zrange->id].zs->get_cmd_by_coords(instance->zones[zrange->id].zs
					, cmd
					, req->a
					, req->e);

			if (!res) {
				CMD_SET_ID(cmd, req->id);
				break;
			}
		}
	}

	return res;
}

static int gss_tia_update_sys_balance(struct gss_instance *instance
					, struct tia_req *req
					, struct paa_cmd *cmd
					, struct tia_req_record *rr)
{
	struct tia_zone *tz = &instance->tia_zone;

	unsigned cmd_duration = cmd_get_duration(cmd);
	unsigned self_balance = ((tz->period - cmd_duration) / req->period) * cmd_duration ;
	unsigned sys_balance = tz->balance;

	if (rr) {
		sys_balance -= tia_req_record_calc_balance(tz, rr);
	}

	if ((tz->quote - sys_balance) < self_balance) {
		pr_err("No room for new request \n");
		return -EINVAL;
	}

	tz->balance += self_balance;

	return 0;
}

static int gss_tia_delete_req(struct gss_instance *instance, struct tia_req *req)
{
	struct tia_req_record *rr;

	if (!(rr = gss_tia_find_req(&instance->tia_zone, req))) {
		pr_n_trace("Non existed request\n");
		return -EINVAL;
	}

	gss_tia_flush_plan(instance);

	list_del(&rr->node);
	instance->tia_zone.balance -= rr->self_balance;

	mempool_free(rr, instance->tia_zone.request_mempool);

	gss_tia_update_plan(instance);

	return 0;
}

/**
 * @brief 
 *
 * @param instance
 * @param req
 *
 * @return 
 */
static int gss_tia_update_req(struct gss_instance *instance, struct tia_req *req)
{
	int res;
	bool new_rr = false;
	bool need_update;
	unsigned req_diff;
	struct tia_req_record *rr;
	struct paa_cmd new_cmd;

	/*
	 * Get TIA command
	 */
	if ((res = gss_get_cmd_by_request(instance, req, &new_cmd))) {
		pr_n_trace("Command parameters can't be obtained.\n");
		return res;
	}

	/*
	 * Get existed or create new request item
	 */
	if (!(rr = gss_tia_find_req(&instance->tia_zone, req))) {
		/* Create new request for serving */
		if(!(rr = mempool_alloc(instance->tia_zone.request_mempool, GFP_KERNEL))) {
			pr_err("Can't allocate memory for new record\n");
			return -ENOMEM;
		} else {
			rr->req = *req;
			rr->cmd = new_cmd;
			new_rr = true;
		}
	}

	/*
	 * Check system time balance
	 */
	if (gss_tia_update_sys_balance(instance, req, &new_cmd, new_rr ? NULL : rr)) {
		/*
		 * TODO: We have to remove requests with lowest priority level
		 */
		if (new_rr)
			mempool_free(rr, instance->tia_zone.request_mempool);
		return -EINVAL;
	}

	/*
	 * Check do we need to re-planning
	 */
	need_update = (new_rr << 0) |
		      ((cmd_get_duration(&new_cmd)
				!= cmd_get_duration(&rr->cmd)) << 1) |
		      ((req_diff = gss_get_request_diff(&rr->req, req, ~0U)) << 2);

	rr->req = *req;
	rr->cmd = new_cmd;
	rr->cmd_time = cmd_get_duration(&rr->cmd);
	rr->self_balance = tia_req_record_calc_balance(&instance->tia_zone, rr);

	if (new_rr)
		gss_tia_insert_rr(&instance->tia_zone, rr);

	if (need_update) {

		if (!new_rr)
			gss_tia_flush_plan(instance);

		gss_tia_update_plan(instance);
	}

	return 0;
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param 
 * @param 
 * @param dnext
 *
 * @return 
 */
static int tia_get_command(struct tia_zone *zone
			, struct timespec *deadline
			, struct paa_cmd_array *cmd_arr
			, unsigned ncmds
			, unsigned *dnext)
{
	unsigned res = 0;
	struct tia_stack_atom *sa;
	struct timespec time_line = *deadline;
	struct paa_cmd *out = &cmd_arr->cmd[cmd_arr->actual_size];

	/*
	 * Check do we have at least one atom
	 */

	if (list_empty(&zone->plan.host_atom.plan_list)) {

		*dnext = ~0U >> 1;
		return 0;
	}

	ts_add_usec(&time_line, *dnext);
	sa = ts_before_or_eq(&atom_start_time(atom_next(&zone->plan.host_atom)), &time_line)
		? atom_next(&zone->plan.host_atom)
		: NULL;

	while (sa && ncmds) {

		while (!list_empty(&sa->host_rec.plan_node)) {

			struct tia_stack_record *sr = list_first_entry(&sa->host_rec.plan_node
								, typeof(*sr)
								, plan_node);

			out[res++] = sr->record->cmd;
			cmd_arr->duration_us += cmd_get_duration(&(sr->record->cmd));

			/*
			 * Move record if needed
			 * TODO: I do not like such algo
			 */
			if (*dnext) {
				ts_sub_usec(&sr->time_planned, *dnext);
				ts_sub_usec(&sr->time_planned_end, *dnext);
			}
			/*
			 * Avoid double planning
			 */

			sr->record->req.next_time = sr->time_real;
			ts_add_usec(&sr->record->req.next_time, sr->record->req.period);

			/*
			 * Update time marker
			 */

			if (likely(ts_before(&time_line, &sr->time_planned_end))) {
				time_line = sr->time_planned_end;
			}

			gss_tia_put_stack_record(zone, sr);

			--ncmds;

			if (!ncmds)
				break;
		}

		if (list_empty(&sa->host_rec.plan_node)) {

			atom_destroy(zone, sa);

			if (unlikely(list_empty(&zone->plan.host_atom.plan_list))) {
				sa = NULL;
			} else {
				sa = atom_next(&zone->plan.host_atom);
				if (ts_after_or_eq(&atom_start_time(sa), &time_line))
					break;
			}
		}
	}

	*deadline = time_line;

	if (ncmds) {
		if (sa) {
			struct timespec dt = timespec_sub(atom_start_time(sa), *deadline);
			*dnext = ts_to_usec(&dt);
		} else {
			*dnext = ~0U >> 1;
		}
	} else {
		if (sa) {
			if (ts_before_or_eq(&atom_start_time(sa), &time_line))
				*dnext = 0;
			else {
				struct timespec dt = timespec_sub(atom_start_time(sa), *deadline);
				*dnext = ts_to_usec(&dt);
			}
		} else {
			*dnext = ~0U >> 1;
		}
	}

	cmd_arr->actual_size += res;

	return res;
}

static int gss_commands_mixer(struct gss_instance *instance
					, struct timespec *deadline
					, struct paa_cmd_array *cmd_arr
					, unsigned ncmds)
{
	int res = 0, ret = 0;
	unsigned next_time = 0, old_next_time = 0;
	struct timespec dl = *deadline;

	ret = ncmds;

	pr_err(" >>> Start mixer. Requested %d commands - %p\n", ncmds, cmd_arr);

	while (ncmds) {

		/*
		 * Get part commands from plan
		 */


		pr_err("\n>> Begin TIA part. DL: %lu.%lu NCMDS:%u NT: %u CARR_LEN: %d CARR_DUR: %d\n"
				, dl.tv_sec, nsec_to_usec(dl.tv_nsec)
				, ncmds, next_time
				, cmd_arr->actual_size, cmd_arr->duration_us);

		ncmds -= (res = tia_get_command(&instance->tia_zone, &dl, cmd_arr, ncmds, &next_time));

		pr_err(">> End TIA part. RES: %d DL: %lu.%lu NCMDS:%u NT: %u CARR_LEN: %d CARR_DUR: %d\n"
				, res
				, dl.tv_sec, nsec_to_usec(dl.tv_nsec)
				, ncmds, next_time
				, cmd_arr->actual_size, cmd_arr->duration_us);
		if (!ncmds)
			break;

		/*
		 * Sanity
		 */

		if (!next_time)
			pr_n_trace("Why we here !?");

		/*
		 * Get commands from review plan
		 */
		pr_err("\n>> Begin TWS part. NCMDS:%u NT: %u CARR_LEN: %d CARR_DUR: %d\n"
				, ncmds, next_time
				, cmd_arr->actual_size, cmd_arr->duration_us);
		old_next_time = next_time;
		ncmds -= (res = gss_get_cmds_timed(&instance->algo, cmd_arr, ncmds, &next_time, NULL));
		pr_err(">> End TWS part. RES:%d NCMDS:%u NT: %u CARR_LEN: %d CARR_DUR: %d\n"
				, res
				, ncmds, next_time
				, cmd_arr->actual_size, cmd_arr->duration_us);

		if (!ncmds)
			break;

		ts_add_usec(&dl, (old_next_time - next_time));
	}

	return ret - ncmds;
}
/**
 * @brief 
 *
 * @param instance
 *
 * @return 
 */
static inline int gss_schedule_update(struct gss_instance *instance)
{
	unsigned i = 0;
	unsigned j = 0;
	struct plan_item *tmp = NULL;

	if (!(instance->plan && instance->num_pitems)) {
		pr_n_trace("Why we here?\n");
		return -EINVAL;
	}

	pr_n_trace("\n\n>>> Reschedule \n");

	instance->tte = 0;
	instance->duration = 0;
	instance->cmds_total = 0;

	if (instance->curr) {
		j = instance->curr->zone->id;
		tmp = flags_is(instance->curr->zone, VALEN) ? instance->curr : NULL;
	}

	for (; i < instance->num_pitems; i++) {
		if (flags_is(instance->plan[j].zone, VALEN)) {
			instance->duration += instance->plan[j].duration;
			instance->cmds_total += instance->plan[j].cmds_total;
			instance->tte += instance->plan[j].tte;

			if (likely(tmp)) {
				tmp = tmp->next = &instance->plan[j];
			}
			else {
				tmp = instance->curr = &instance->plan[j];
			}
		}
		pr_err("J = %d\n", j);

		j++;
		j %= instance->num_pitems;
	}

	pr_err(">> Duration: %d, CMDS: %d \n", instance->duration, instance->cmds_total);

	return 0;
}

/**
 * @brief 
 *
 * @param algo
 * @param sched
 * @param id
 *
 * @return 
 */
static int gss_zone_add(struct global_schedulder_algo *algo, struct zone_scheduler *sched, uint32_t id)
{
	int ret = 0;
	unsigned i = 0;
	struct gss_instance *instance = algo_to_inst(algo);
	unsigned long flags;
	struct zone_range *zrange, *zrange_pivot;

	spin_lock_irqsave(&instance->lock, flags);

	if (id >= MAX_ZONES_NUMBER) {
		pr_err("MAX_ZONES_NUMBER id %d but proposed id is %d. Rejected\n"
				, MAX_ZONES_NUMBER
				, id);
		ret = -EINVAL;
		goto exit;
	}

	if (instance->zones[id].flags & ZRF_VALID) {
		pr_err("Zone with Id=%d already present\n", id);
		ret =  -EINVAL;
		goto exit;
	}

	if (sched->get_stats) {
		sched->get_stats(sched, &instance->zones[id].stats);
	} else {
		pr_err("Not get_stats callback in zone %d\n", id);
		ret =  -EINVAL;
		goto exit;
	}

	instance->zones[id].id = id;
	instance->zones[id].flags = ZRF_VALID | ZRF_ENABLED;
	instance->zones[id].zs = sched;

	for (; i < instance->num_pitems; i++) {
		if (instance->plan[i].zone->id ^ id)
			continue;

		instance->plan[i].zone->zs->layer_to_cmd(instance->plan[i].zone->zs
				, instance->plan[i].val
				, &instance->plan[i].cmds_start
				, &instance->plan[i].cmds_end
				, &instance->plan[i].duration);

		instance->plan[i].cmds_left = instance->plan[i].cmds_total
			= instance->plan[i].cmds_end
				- instance->plan[i].cmds_start
				+ 1;
	}

	/*
	 * Add zone node to sorted list
	 */
	zrange = kmalloc(sizeof(*zrange), GFP_KERNEL);
	zrange->id = id;
	zrange->range = instance->zones[id].stats.d_range[1];

	INIT_LIST_HEAD(&zrange->node);

	list_for_each_entry(zrange_pivot, &instance->zones_by_range, node) {
		if (zrange_pivot->range >= zrange->range)
			break;
	}

	list_add_tail(&zrange->node, &instance->zones_by_range);

	gss_schedule_update(instance);

exit:
	spin_unlock_irqrestore(&instance->lock, flags);

	return ret;
}

/**
 * @brief 
 *
 * @param algo
 * @param id
 */
static void gss_zone_remove(struct global_schedulder_algo *algo, uint32_t id)
{
	unsigned long flags;
	struct gss_instance *instance = algo_to_inst(algo);

	spin_lock_irqsave(&instance->lock, flags);

	if (id >= MAX_ZONES_NUMBER) {
		pr_err("MAX_ZONES_NUMBER id %d but proposed id is %d. Rejected\n"
				, MAX_ZONES_NUMBER
				, id);
		goto exit;
	}

	instance->zones[id].flags &= ~ZRF_VALID;

	gss_schedule_update(instance);

exit:
	spin_unlock_irqrestore(&instance->lock, flags);
}

/**
 * @brief 
 *
 * @param algo
 * @param id
 * @param en
 */
static void gss_zone_enable(struct global_schedulder_algo *algo
				, uint32_t id
				, unsigned en)
{
	unsigned long flags;
	struct gss_instance *instance = algo_to_inst(algo);

	spin_lock_irqsave(&instance->lock, flags);

	if (!(instance->zones[id].flags & ZRF_VALID)) {
		pr_err("Zone %d is not valid\n", id);
		return;
	}

	if (((instance->zones[id].flags & ZRF_ENABLED) && en) ||
		(!((instance->zones[id].flags & ZRF_ENABLED) || en)))
		return;

	if (en)
		instance->zones[id].flags |= ZRF_ENABLED;
	else
		instance->zones[id].flags &= ~ZRF_ENABLED;

	gss_schedule_update(instance);

	spin_unlock_irqrestore(&instance->lock, flags);
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param count
 *
 * @return 
 */
static int gss_get_cmds(struct global_schedulder_algo *algo
				, struct paa_cmd_array *cmds
				, uint32_t count)
{
	uint32_t old_flags = cmds->flags;
	unsigned req, ret;
	unsigned old_duration, old_asize = 0;
	unsigned long flags;
	struct gss_instance *instance = algo_to_inst(algo);

	if (!(cmds && count))
		return 0;

	if (!(cmds->flags & CAF_APPEND)) {
		cmds->actual_size = 0;
		cmds->duration_us = 0;
		cmds->flags = CAF_APPEND;
	} else {
		old_asize = cmds->actual_size;
	}

	spin_lock_irqsave(&instance->lock, flags);

	do {
		req = instance->curr->cmds_left > count
			? count : instance->curr->cmds_left;

		old_duration = cmds->duration_us;

		if (!(ret = instance->curr->zone->zs->get_cmds(instance->curr->zone->zs, cmds, req, NULL))) {
			pr_err("Returned zero commands, but requested %d\n", req);
			goto exit;
		}

		if (ret ^ req)
			pr_err("Requested commands number and returned are different\n");

		if(!(instance->curr->cmds_left -= ret)) {
			pr_err(" >>> switch to next item. Current: %lu\n"
					, instance->curr - instance->plan);
			/* restore plan item cmds counter and TTE  */
			instance->curr->cmds_left = instance->curr->cmds_total;
			instance->curr->tte = instance->curr->duration;

			/* restore global TTE value */
			if (instance->curr >= instance->curr->next) {
				instance->tte = instance->duration;
				instance->cmds_rem = instance->cmds_total;
			} else {
				instance->tte -= (cmds->duration_us - old_duration);
				instance->cmds_rem -= ret;
			}

			/* switch to next plan item */
			instance->curr = instance->curr->next;

			pr_err(" >>> switch to next item. Next: %lu\n"
					, instance->curr - instance->plan);

			/* perform seek for start command in zone */
			instance->curr->zone->zs->seek(instance->curr->zone->zs
					,instance->curr->cmds_start);
		} else {
			/* just decrease comds counter and TTE */
			instance->curr->tte -= (cmds->duration_us - old_duration);
			instance->tte -= (cmds->duration_us - old_duration);
			instance->cmds_rem -= ret;
		}
		pr_err("  >>> Count: %d ret: %d\n", count, ret);
	} while(count -= ret);

	pr_err(" Remaining %d commands and %d us\n", instance->cmds_rem,
						instance->tte);

exit:
	spin_unlock_irqrestore(&instance->lock, flags);

	cmds->flags = old_flags;

	return cmds->actual_size - old_asize;
}

/**
 * @brief 
 *
 * @param 
 * @param 
 * @param duration_us
 *
 * @return 
 */
static int gss_process_req(struct global_schedulder_algo *algo
						, struct tia_req *request)
{
	unsigned long flags;
	struct gss_instance *instance = algo_to_inst(algo);

	spin_lock_irqsave(&instance->lock, flags);

	/* Get zone and scan parameters */
	switch (request->flags) {

	case REQ_INSERT:
	case REQ_UPDATE:

		gss_tia_update_req(instance, request);
		break;

	case REQ_DELETE:

		gss_tia_delete_req(instance, request);
		break;
	}

	spin_unlock_irqrestore(&instance->lock, flags);

	return 0;
}


/**
 * @brief 
 *
 * @param 
 * @param 
 * @param duration_us
 *
 * @return 
 */
static int gss_get_cmds_timed(struct global_schedulder_algo *algo
						, struct paa_cmd_array *cmds
						, unsigned count
						, unsigned *duration_us
						, unsigned *next_duration)
{
	struct gss_instance *instance = algo_to_inst(algo);
	unsigned req, ret;
	unsigned old_duration, old_asize = cmds->actual_size;

	do {
		req = instance->curr->cmds_left > count
			? count : instance->curr->cmds_left;

		old_duration = cmds->duration_us;

//		pr_err("old_d = %lu, duration_us = %lu \n", old_duration, *duration_us);

		ret = instance->curr->zone->zs->get_cmds(instance->curr->zone->zs
								, cmds
								, req
								, duration_us);
//		pr_err("cmds->d = %lu, ret = %lu \n", cmds->duration_us, ret);

		if(!(instance->curr->cmds_left -= ret)) {

			/* restore plan item cmds counter and TTE  */

			instance->curr->cmds_left = instance->curr->cmds_total;
			instance->curr->tte = instance->curr->duration;

			/* restore global TTE value */

			if (instance->curr >= instance->curr->next) {
				instance->tte = instance->duration;
				instance->cmds_rem = instance->cmds_total;
			} else {
				instance->tte -= (cmds->duration_us - old_duration);
				instance->cmds_rem -= ret;
			}

			/* switch to next plan item */
			instance->curr = instance->curr->next;

			/* perform seek for start command in zone */
			instance->curr->zone->zs->seek(instance->curr->zone->zs
						,instance->curr->cmds_start);
		} else {
			/* just decrease comds counter and TTE */

			instance->curr->tte -= (cmds->duration_us - old_duration);
			instance->tte -= (cmds->duration_us - old_duration);
			instance->cmds_rem -= ret;

			if (ret ^ req)
				break;
		}

	} while(count -= ret);

	return cmds->actual_size - old_asize;
}

/**
 * @brief 
 *
 * @param algo
 *
 * @return 
 */
static uint32_t gss_time_to_end(struct global_schedulder_algo *algo)
{

	return 0;
}

/**
 * @brief 
 *
 * @param instance
 */
inline static void gss_plan_free(struct gss_instance *instance)
{
	if (!instance->plan)
		return;

	kfree(instance->plan);

	instance->plan = NULL;
	instance->curr = NULL;
}

/**
 * @brief 
 *
 * @param plan
 *
 * @return 
 */
static int gss_add_plan(struct gss_instance *instance
				, struct gss_review_plan const *plan)
{
	struct plan_item *tmp_item = NULL;
	unsigned long flags;
	unsigned i = 0;

	if (!plan->items_cnt) {
		pr_err("No items in review plan\n");
		return -EINVAL;
	}

	spin_lock_irqsave(&instance->lock, flags);

	gss_plan_free(instance);

	if (!(instance->plan = kzalloc(plan->items_cnt * sizeof *instance->plan
					, GFP_KERNEL))) {
		pr_err("Out of memory here: %s: %d\n", __func__, __LINE__);
		return -ENOMEM;
	}

	instance->num_pitems = plan->items_cnt;

	pr_err("Items = %d\n", plan->items_cnt);

	for(; i < plan->items_cnt; i++) {
		instance->plan[i].zone = &instance->zones[plan->items[i].zone_id];

		instance->plan[i].val[0] = plan->items[i].data.cmds[0];
		instance->plan[i].val[1] = plan->items[i].data.cmds[1];

		pr_err("Process plan item: %d zone:%d s:%d e:%d\n"
							, i
							, plan->items[i].zone_id
							, instance->plan[i].val[0]
							, instance->plan[i].val[1]);

		if (!(instance->plan[i].zone->flags & ZRF_VALID))
			continue;

		pr_err("Adding zone %d to plan\n", instance->plan[i].zone->id);

		instance->plan[i].zone->zs->layer_to_cmd(instance->plan[i].zone->zs
				, instance->plan[i].val
				, &instance->plan[i].cmds_start
				, &instance->plan[i].cmds_end
				, &instance->plan[i].duration);

		instance->plan[i].cmds_left = instance->plan[i].cmds_total
			= instance->plan[i].cmds_end
				- instance->plan[i].cmds_start
				+ 1;

		pr_err("Plan duration: cmds %d time: %d\n"
				, instance->plan[i].cmds_left
				, instance->plan[i].duration);

		if ((instance->plan[i].zone->flags & ZRF_ENABLED)) {
			if (likely(tmp_item)) {
				tmp_item->next = &instance->plan[i];
				tmp_item = tmp_item->next;

			} else {
				instance->curr = tmp_item = &instance->plan[i];
			}
			instance->duration += instance->plan[i].duration;
			instance->cmds_total += instance->plan[i].cmds_total;
		}
	}

	instance->tte = instance->duration;
	instance->cmds_rem = instance->cmds_total;

	if (tmp_item)
		tmp_item->next = instance->curr;

	spin_unlock_irqrestore(&instance->lock, flags);

	return 0;
}

/**
 * @brief 
 *
 * @param algo
 * @param var
 *
 * @return 
 */
static int gss_ioctl(struct global_schedulder_algo *algo, struct gs_var *var)
{
	int ret = 0;
	struct gss_instance *instance = algo_to_inst(algo);

	if (!var) {
		pr_err("NULL value \n");
		return -EINVAL;
	}

	pr_err("VAR: %d\n", var->type);

	switch (var->type) {

	case GSSETPLAN:
		gss_add_plan(instance,
				(struct gss_review_plan const *) var->data);
		break;

	case GSTESTMIXER:
		pr_err("Call mixer\n");
		ret = gss_commands_mixer(instance
					, &((struct gss_mixer_test *)var->data)->deadline
					, ((struct gss_mixer_test *)var->data)->cmd_arr
					, ((struct gss_mixer_test *)var->data)->ncmds);
		break;

	default:
		pr_err("Incorrect GS IOCTL: %d\n", var->type);
		ret = -EIO;
		break;
	}

	return ret;
}


static void tia_req_ctor(void *data)
{
	struct tia_req_record *req_record = data;

	memset(data, 0x00, sizeof *req_record);

	INIT_LIST_HEAD(&req_record->node);
	INIT_LIST_HEAD(&req_record->pstack);
}

struct proc_ops_specific {
	__u32 id;
	int (*show)(struct seq_file *s, void *unused);
};


static int proc_plan_dump_requests_gp(struct seq_file *s, void *unused)
{
	struct tia_zone *tz = &instance.tia_zone;

	dump_requests_gp(tz, s);

	return 0;
}

static int proc_plan_dump_requests(struct seq_file *s, void *unused)
{
	struct tia_zone *tz = &instance.tia_zone;

	dump_requests(tz, s);

	return 0;
}

static int proc_plan_dump_atoms(struct seq_file *s, void *unused)
{
	struct tia_zone *tz = &instance.tia_zone;

	dump_plan(tz, 0, s);

	return 0;
}

static int proc_show_version(struct seq_file *s, void *unused)
{
	seq_printf(s, "Version: %d.%d\n", GS_VERSION_MAJOR, GS_VERSION_MINOR);
	return 0;
}

static int proc_file_open(struct inode *inode, struct file *file)
{
	struct proc_dir_entry *pde = PDE(inode);
	struct proc_ops_specific *data = pde->data;

	single_open(file, data->show, data);
	return 0;
}

static struct file_operations fops = {
	.open = proc_file_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

static struct proc_ops_specific proc_file_ops[] = {

	[PROC_VERSION] = {
		.id = PROC_VERSION,
		.show = proc_show_version,
	},

	[PROC_PLAN_ATOMS_DUMP] = {
		.id = PROC_PLAN_ATOMS_DUMP,
		.show = proc_plan_dump_atoms,
	},

	[PROC_PLAN_REQUESTS_DUMP] = {
		.id = PROC_PLAN_REQUESTS_DUMP,
		.show = proc_plan_dump_requests,
	},

	[PROC_PLAN_REQUESTS_DUMP_GP] = {
		.id = PROC_PLAN_REQUESTS_DUMP_GP,
		.show = proc_plan_dump_requests_gp,
	},
};

static int gss_init(struct global_schedulder_algo *algo)
{
	struct gss_instance *instance = algo_to_inst(algo);
	struct timespec plan_time = {10, 0};

	instance->tia_zone.request_cache = kmem_cache_create("tia_req"
						, sizeof(struct tia_req_record)
						, 0
						, SLAB_HWCACHE_ALIGN
						, tia_req_ctor);

	instance->tia_zone.request_mempool = mempool_create(15
						, mempool_alloc_slab
						, mempool_free_slab
						, instance->tia_zone.request_cache);

	gss_tia_create_plan(instance, &plan_time, 20 * 1000 * 1000, true);

	if (instance->proc_entries[PROC_PARENT].entry) {
		instance->proc_entries[PROC_VERSION].entry = proc_create_data(proc_names[PROC_VERSION]
						, 0600
						, (instance->proc_entries[PROC_VERSION].parent
							= instance->proc_entries[PROC_PARENT].entry)
						, &fops
						, &proc_file_ops[PROC_VERSION]);

		instance->proc_entries[PROC_PLAN].entry = proc_mkdir(proc_names[PROC_PLAN]
						, (instance->proc_entries[PROC_PLAN].entry
							= instance->proc_entries[PROC_PARENT].entry));

		instance->proc_entries[PROC_PLAN_ATOMS_DUMP].entry = proc_create_data(proc_names[PROC_PLAN_ATOMS_DUMP]
						, 0600
						, (instance->proc_entries[PROC_PLAN_ATOMS_DUMP].parent
							= instance->proc_entries[PROC_PLAN].entry)
						, &fops
						, &proc_file_ops[PROC_PLAN_ATOMS_DUMP]);

		instance->proc_entries[PROC_PLAN_REQUESTS_DUMP].entry = proc_create_data(proc_names[PROC_PLAN_REQUESTS_DUMP]
						, 0600
						, (instance->proc_entries[PROC_PLAN_REQUESTS_DUMP].parent
							=  instance->proc_entries[PROC_PLAN].entry)
						, &fops
						, &proc_file_ops[PROC_PLAN_REQUESTS_DUMP]);

		instance->proc_entries[PROC_PLAN_REQUESTS_DUMP_GP].entry = proc_create_data(proc_names[PROC_PLAN_REQUESTS_DUMP_GP]
						, 0600
						, (instance->proc_entries[PROC_PLAN_REQUESTS_DUMP_GP].parent
							=  instance->proc_entries[PROC_PLAN].entry)
						, &fops
						, &proc_file_ops[PROC_PLAN_REQUESTS_DUMP_GP]);

		instance->proc_entries[PROC_ZONES].entry = proc_mkdir(proc_names[PROC_ZONES]
						, instance->proc_entries[PROC_PARENT].entry);
	}

	return 0;
}

static struct gss_instance instance = {
	.algo = {
		.name = GS_SIMPLE_NAME,
		.init = gss_init,
		.ioctl = gss_ioctl,
		.zone_add = gss_zone_add,
		.zone_remove = gss_zone_remove,
		.zone_enable = gss_zone_enable,
		.get_cmds = gss_get_cmds,
		.get_cmds_timed = gss_get_cmds_timed,
		.process_req = gss_process_req,
		.time_to_end = gss_time_to_end,
	},

	.tia_zone = {
		.min_interval = DEF_DEFRAG_INTERVAL,
		.period = DEF_PERIOD_LEN,
		.quote = DEF_PERIOD_LEN * DEF_TIA_PERCENTS,
		.plan = {
			.host_atom = {
				.plan_list = {
					.next = &instance.tia_zone.plan.host_atom.plan_list,
					.prev = &instance.tia_zone.plan.host_atom.plan_list,
				},
				.host_rec = {
					.id = HEAD_POISON,
					.time_real = {0, 0},
					.time_planned = {~0UL >> 1, ~0UL >> 1},
					.time_planned_end = {0, 0},
					.record = NULL,
					.stack_node = {
						.next = &instance.tia_zone.plan.host_atom.host_rec.stack_node,
						.prev = &instance.tia_zone.plan.host_atom.host_rec.stack_node,
					},
					.plan_node = {
						.next = &instance.tia_zone.plan.host_atom.host_rec.plan_node,
						.prev = &instance.tia_zone.plan.host_atom.host_rec.plan_node,
					},
				},
			},
		},

		.request = LIST_HEAD_INIT(instance.tia_zone.request),

		.pstack_cache = LIST_HEAD_INIT(instance.tia_zone.pstack_cache),

		.patom_cache = LIST_HEAD_INIT(instance.tia_zone.patom_cache),
	},

	.zones = {},
	.zones_by_range = LIST_HEAD_INIT(instance.zones_by_range),
	.proc_entries = {{NULL, NULL}, },
};

static int __init gs_init(void)
{
	spin_lock_init(&instance.lock);

	gs_algo_register(&instance.algo, &(instance.proc_entries[PROC_PARENT].entry));

	return 0;
}

static void __exit gs_exit(void)
{
	int i = PROC_PARENT + 1;

	/*
	 * Clean procfs entries
	 */

	for (; i < PROC_LAST; i++) {

//		if (instance.proc_entries[i].entry) {
//			remove_proc_entry(proc_names[i], instance.proc_entries[i].parent);
//		}

	}

	gs_algo_unregister(GS_SIMPLE_NAME);

}

module_init(gs_init);
module_exit(gs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ryazantsev Vladimir <riazantsev.v@gmail.com>");
MODULE_DESCRIPTION("Global Scheduler: Simple");

